#![feature(plugin, custom_derive)]
#![plugin(rocket_codegen)]

extern crate rocket;
#[macro_use]
#[macro_use]
extern crate bson;
extern crate mongodb;
extern crate rocket_contrib;
extern crate serde;
extern crate temp;
#[macro_use]
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
extern crate geojson;
extern crate time;

use mongodb::db::ThreadedDatabase;
use mongodb::db::Database;
use mongodb::{Client, ThreadedClient};
use mongodb::coll::Collection;
use rocket::request::Form;
use rocket::response::NamedFile;
use rocket_contrib::Json;
use rocket_contrib::Template;
use temp::core::models::District;
use temp::core::models::Game;
use geojson::GeoJson;
use geojson::Geometry;
use temp::core::models::Position;
use temp::core::models::Score;
use std::collections::HashMap;
use temp::mail::{Email, Mailbox, MailAddress};
use temp::mail::OutgoingMailbox;
use std::path::{Path, PathBuf};

use rocket::response::status;
//use r2d2::{Pool, PooledConnection};
//use r2d2_mongodb::{ConnectionOptionsBuilder, MongodbConnectionManager};
//use std::ops::Deref;
use rocket::State;
use rocket::Request;
use rocket::request;
use rocket::request::FromRequest;
use rocket::http::Status;

use temp::config::Configuration;
use temp::db::CollectionInterface;
use temp::db::MongoCollection;

mod parse_bezirke;
use parse_bezirke::parse_bezirke;
/*

   pub struct DbConn(pub PooledConnection<MongodbConnectionManager>);

   impl<'a, 'r> FromRequest<'a, 'r> for DbConn {
   type Error = ();

   fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
   let pool = request.guard::<State<Pool<MongodbConnectionManager>>>()?;
   match pool.get() {
   Ok(conn) => Outcome::Success(DbConn(conn)),
   Err(_) => Outcome::Failure((Status::ServiceUnavailable, ()))
   }
   }
   }

// For the convenience of using an &DbConn as an &SqliteConnection.
impl Deref for DbConn {
type Target = PooledConnection<MongodbConnectionManager>;

fn deref(&self) -> &Self::Target {
&self.0
}
}

fn pool() -> Pool<MongodbConnectionManager> {
let loader = setup_config();

let options = match loader.database.auth {
Some(auth) => {
ConnectionOptionsBuilder::new()
.with_host(&loader.database.host.unwrap_or("localhost".to_string()))
.with_username(&auth.username)
.with_password(&auth.password)
.with_port(loader.database.port.unwrap_or(27017))
.with_db(&loader.database.database)
.build()
}
None => {
ConnectionOptionsBuilder::new()
.with_host(&loader.database.host.unwrap_or("localhost".to_string()))
.with_port(loader.database.port.unwrap_or(27017))
.with_db(&loader.database.database)
.build()
}
};

let manager = MongodbConnectionManager::new(options);

let pool = Pool::builder()
.max_size(8)
.build(manager)
.unwrap();

pool
}
*/

fn setup_pool(config: &Configuration) -> MongoCollection {
    let client = Client::connect(&config.database.host.clone().unwrap_or("localhost".to_string()), config.database.port.unwrap_or(27017))
        .expect("Failed to initialize standalone client.");

    MongoCollection(client.db(&config.database.database).collection("games"))
}

fn setup_config() -> Configuration {
    Configuration::load_config("configuration.toml").expect("Configuration.toml was not found")
}

#[derive(Serialize)]
struct TemplateContext {
    game: Game,
    scores: Vec<Score>
}

fn get_game(coll: &MongoCollection, id: String) -> Result<Game, status::NotFound<String>> {
    let doc =
        coll.find_one(
            doc! {
                "_id":  bson::oid::ObjectId::with_string(&id).map_err(|_| status::NotFound(format!("{} is not a valid id", id)))?
            },
            )
        .ok_or(status::NotFound(format!("Document with the id {} was not found", id)))?;
    //.map_err(|_| status::NotFound(format!("Could not fetch from the database")))?

    let doc = bson::from_bson(bson::Bson::Document(doc)).map_err(|err| status::NotFound(format!("Cannot parse document to struct: {}", err))).unwrap();

    Ok(doc)
}

#[get("/")]
fn index() -> Template {
    use std::collections::HashMap;

    let context: HashMap<(), ()> = HashMap::new();
    Template::render("index", context)
}

#[get("/game/<id>")]
fn render_game(id: String) -> Result<Template, status::NotFound<String>> {
    let coll = setup_pool(&setup_config());
    let mut de_game = get_game(&coll, id)?;
    de_game.sort_map();

    let scores : Vec<Score> = de_game.get_scores();

    Ok(Template::render("game", TemplateContext { game: de_game, scores: scores }))
}

#[get("/api/game/<id>")]
fn api_get_game(id: String) -> Result<Json<Game>, status::NotFound<String>> {
    let coll = setup_pool(&setup_config());
    let mut de_game = get_game(&coll, id)?;
    de_game.queue.clear();

    Ok(Json(de_game))
}

#[get("/api/map/<id>")]
fn api_get_map(id: String) -> Result<Json<Vec<District>>, status::NotFound<String>> {
    let coll = setup_pool(&setup_config());

    let de_game = get_game(&coll, id)?;

    Ok(Json(de_game.map))
}

#[get("/api/bezirke/<id>")]
fn api_get_bezirke(id: String) -> Result<Json<GeoJson>, status::NotFound<String>> {
    let coll = setup_pool(&setup_config());

    let de_game = get_game(&coll, id)?;
    let mut geojson = parse_bezirke();

    match geojson {
        GeoJson::FeatureCollection(ref mut ctn) => {
            for feature in &mut ctn.features {
                if let Some(ref mut properties) = feature.properties {
                    let name = match properties.get("name").expect("Geojson property has no name") {
                        serde_json::Value::String(ref v) => v.clone(),
                        _ => unreachable!("json is invalid")
                    };

                    let district = de_game.find_district(&name).unwrap();

                    let values = district.values.iter().map(|party_presence| {
                        let mut map = ::serde_json::Map::new();

                        map.insert("party".to_string(), ::serde_json::Value::String(party_presence.name.clone()));
                        map.insert("color".to_string(), ::serde_json::Value::String(party_presence.color.clone()));
                        map.insert("value".to_string(), ::serde_json::Value::Number(::serde_json::Number::from(party_presence.value)));

                        ::serde_json::Value::Object(map)
                    }).collect();

                    properties.insert("values".to_string(), ::serde_json::Value::Array(values));


                    //feature.properties.values = Some(district.values.clone());
                }
            }

            //Add figures
            let figures : Vec<_> = de_game.members.iter().map(|user| &user.figures).flatten().collect();
            let mut saved_figures : Vec<_> = Vec::new();

            for db_figure in figures.iter() {

                let figures =
                    r#"
                    {
                        "type": "Feature",
                        "properties": {
                            "name": ""
                        },
                        "geometry": {
                            "type": "Point",
                            "coordinates": [0,0]
                        }
                    }
                    "#;

                    let mut gfeatures = figures.parse::<GeoJson>().unwrap();

                    //Find coordinates
                    for feature in &mut ctn.features {
                        let district_name = match feature.properties {
                            Some(ref mut prop) => {
                                match prop.get("name").expect("Geojson feature has no name property") {
                                    serde_json::Value::String(ref v) => v.clone(),
                                    _ => unreachable!("json is invalid")
                                }
                            }
                            None => {
                                unreachable!("Geojson is none");
                            }
                        };

                        if district_name == db_figure.district.clone() {
                            match gfeatures {
                                GeoJson::Feature(ref mut f) => {
                                    let geometry = feature.geometry.clone().expect("Feature has no geometry");
                                    //geometry.insert("type", ::serde_json::Value::String("Point"));
                                    //f.geometry = Some(geometry);

                                    if let Some(ref mut prop) = f.properties {
                                        prop.insert("district_name".to_string(), ::serde_json::Value::String(district_name));
                                        prop.insert("figure".to_string(), ::serde_json::Value::String(format!("{:?}", db_figure.figure)));
                                        prop.insert("color".to_string(), ::serde_json::Value::String(format!("{:?}", db_figure.color)));
                                        prop.insert("party".to_string(), ::serde_json::Value::String(format!("{:?}", db_figure.party)));
                                        prop.insert("coords".to_string(), ::serde_json::to_value(&geometry).unwrap());

                                        //TODO here icons
                                    }

                                    saved_figures.push(f.clone());
                                }
                                _ => {
                                    unreachable!("Geojson is not a feature");
                                }
                            }
                        }
                    }

                    ctn.features.append(&mut saved_figures);
            }

        }
        _ => unreachable!("Geojson is not a feature collection")
    }

    Ok(Json(geojson))
}

#[get("/game/start/<id>")]
fn start_game(id: String) -> Result<&'static str, status::NotFound<String>> {
    let coll = setup_pool(&setup_config());

    let mut de_game = get_game(&coll, id)?;

    //de_game.start().map_err(|_| status::NotFound(format!("Cannot start the game because it has already started")))?;
    de_game.start().map_err(|_| status::NotFound(format!("Ich kann neues Spiel starten, weil es bereits läuft - Good luck")))?;

    //Updating the db
    let filter = doc! {
        "_id": (de_game.id.clone().unwrap())
    };

    // - Serialize
    let ser_game = bson::to_bson(&de_game).map_err(|_| status::NotFound(format!("Cannot parse struct to bson")))?;

    if let bson::Bson::Document(document) = ser_game {
        coll.replace_one(filter, document).map_err(|_| status::NotFound(format!("Cannot replace document in the database")))?;
    } else {
        unreachable!("bson is not a document");
    }

    Ok("ok")
}

#[derive(FromForm)]
struct User {
    pub email: String,
    pub token: String,
}

#[post("/user", data = "<user>")]
fn new_user(user: Form<User>, loader: State<Configuration>) -> Result<Template, status::NotFound<String>> {
    let form = user.get();
    let email = form.email.clone().trim().to_string();
    let token = form.token.clone().trim().to_string();

    let coll = setup_pool(&setup_config());

    //Fetching game
    let mut de_game = get_game(&coll, token.clone())?;

    //Update the model

    let user = de_game.add_user(&coll, email.clone()).map_err(|err| status::NotFound(format!("{}", err)))?;

    //Updating the db
    let filter = doc! {
        "_id": bson::oid::ObjectId::with_string(&token).map_err(|_| status::NotFound(format!("Token is invalid")))?
    };

    // - Serialize
    //let ser_game = bson::to_bson(&de_game).map_err(|_| status::NotFound(format!("Cannot parse struct to bson")))?;
    let ser_user = bson::to_bson(&user).map_err(|err| status::NotFound(format!("Cannot parse struct to bson: {}", err)))?;

    if let bson::Bson::Document(document) = ser_user {
        let mut update = doc! {
            "$push": {
                "members": {
                    "$each": []
                }
            }
        };

        match update.get_mut("$push").unwrap() {
            bson::Bson::Document(ref mut doc) => {
                match doc.get_mut("members").unwrap() {
                    bson::Bson::Document(ref mut doc) => {
                        match doc.get_mut("$each").unwrap() {
                            bson::Bson::Array(ref mut arr) => {
                                arr.push(bson::Bson::Document(document));
                            }
                            _ => unreachable!("Bson is not an array")
                        }
                    }
                    _ => unreachable!("Bson is not a document")
                }
            }
            _ => unreachable!("Bson is not a document")
        }

        coll.update_one(filter, update).map_err(|err| status::NotFound(format!("Cannot update game in the database: {}", err)))?;

        let mailbox = Mailbox::new(loader.get_mails()[0].clone());

        mailbox.send_raw_mail(Email {
            id: MailAddress("".to_string()),
            subject: "Spiel Join".to_string(),
            body: format!(r#"
                                    Hallo!

                                    Du bist dem Spiel beigetreten! Ich warte nur noch bis der Admin das Spiel startet.

                                    Informationen zum Spiel kannst du unter folgender Seite bekommen:
                                    {}/game/{}
                                    Diese wird täglich um Mitternacht upgedatet.

                                    Unter {}/readme kannst die Regeln zum Spiel nachlesen.


                                "#, loader.get_addr(), token.clone(), loader.get_addr()),
                                parent: None,
                                date: None,
                                from: MailAddress(loader.get_mails()[0].clone().email),
                                to: MailAddress(form.email.clone()),
        }).map_err(|err| status::NotFound(format!("Cannot send email to user\n{}", err)))?;

        let other_members: Vec<_> = de_game.members.iter().filter(|u| u.email != email).collect();

        println!("{:?}", other_members);

        for u in other_members.iter() {

            mailbox.send_raw_mail(Email {
                id: MailAddress("".to_string()),
                subject: "Spieler beigetreten".to_string(),
                body: format!(r#"
                Hallo!

                Spieler {} ist dem Spiel beigetreten!
            "#, user.email.clone()),
            parent: None,
            date: None,
            from: MailAddress(loader.get_mails()[0].clone().email),
            to: MailAddress(u.email.clone())
            }).map_err(|err| status::NotFound(format!("Cannot send email to user\n{}", err)))?;

        }

        let mut hashmap = HashMap::new();
        hashmap.insert("token", token.clone());

        Ok(Template::render("join", hashmap))
    } else {
        unreachable!("Bson is not a document")
    }
}

#[derive(FromForm)]
pub struct NewGame {
    pub email: String
}

#[post("/game", data="<new_game>")]
fn new_game(new_game: Form<NewGame>, loader: State<Configuration>) -> Result<Template, status::NotFound<String>> {
    let form = new_game.into_inner();
    let mut game = Game::new(form.email.clone());

    let coll = setup_pool(&setup_config());

    let user = game.add_user(&coll, form.email.clone()).map_err(|err| status::NotFound(format!("{}", err)))?;
    game.members.push(user);

    let mut ser_game = bson::to_bson(&game).map_err(|_| status::NotFound(format!("Cannot parse struct to bson")))?;

    if let bson::Bson::Document(document) = ser_game {
        let id = coll
            .insert_one(document)
            .map_err(|err| status::NotFound(format!("Cannot insert into database\n{}", err)))?.to_hex();


        let mailbox = Mailbox::new(loader.get_mails()[0].clone());
        mailbox.send_raw_mail(Email {
            id: MailAddress("".to_string()),
            subject: "Neues Spiel gestartet".to_string(),
            body: format!(r#"
                                      Hallo!

                                      Dein Spiel wurde erstellt!
                                      Du kannst jetzt deine Leute dazu einladen, dass sie bei deinem Spiel teilnehmen.
                                      Das/der Token dafür lautet: {}

                                      Sobald alle Leute eingetragen sind, kannst du mir "$ Spiel starten" schreiben - dann geht's los!

                                      Informationen zum Spiel kannst du unter folgender Seite bekommen:
                                      {}/game/{}
                                      Diese wird täglich um Mitternacht upgedatet.

                                      Unter {}/readme kannst die Regeln zum Spiel nachlesen.

                                      "#, id.clone(), loader.get_addr(), id.clone(), loader.get_addr().clone()),
                                      parent: None,
                                      date: None,
                                      from: MailAddress(loader.get_mails()[0].clone().email),
                                      to: MailAddress(form.email.clone()),
        }).map_err(|err| status::NotFound(format!("Cannot send email to user\n{}", err)))?;

        let mut hashmap = HashMap::new();
        hashmap.insert("token", id);

        Ok(Template::render("token_display", hashmap))
    } else {
        let mut hashmap = HashMap::new();
        hashmap.insert("error", "Ich kann nicht das Token anzeigen");

        Ok(Template::render("error", hashmap))
    }
}

#[get("/readme")]
fn readme() -> Template {
    let mut context : HashMap<(), ()> = HashMap::new();
    Template::render("readme", context)
}

#[get("/static/<file..>")]
fn files(file: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("static/").join(file)).ok()
}

#[error(500)]
fn internal_error() -> Template {
    let mut context = HashMap::new();
    context.insert("error".to_string(), "Whoops! Looks like we messed up.".to_string());

    Template::render("error", context)
}

#[error(404)]
fn not_found(req: &Request) -> Template {
    let mut context = HashMap::new();
    context.insert("error".to_string(), "Seite nicht gefunden".to_string());

    Template::render("error", context)

}

fn main() {
    rocket::ignite()
        //.manage(pool())
        .manage(setup_config())
        .mount(
            "/",
            routes![
            index,
            new_user,
            new_game,
            render_game,
            start_game,
            api_get_game,
            api_get_map,
            api_get_bezirke,
            files,
            readme
            ],
            )
        .attach(Template::fairing())
        /*.catch(errors! [
          internal_error,
          not_found
          ])
          */
        .launch();
}
