use config::MailConfiguration;
use failure;
use failure::err_msg;
use imap;
use std::sync::mpsc;

use chrono::prelude::*;
use core::pipeline::StageManager;
use mail::{Email, Mailbox};
use mongodb::{Client, ThreadedClient};
use native_tls::TlsConnector;
use std::collections::HashMap;
use std::sync::mpsc::Receiver;
use std::sync::{Arc, Mutex};
use std::thread;
use mongodb::db::ThreadedDatabase;
use db::MongoCollection;
use mail::OutgoingMailbox;

type Error = failure::Error;
type EmailAddress = String;

//TODO using a db pool
pub struct Server {
    mails: HashMap<EmailAddress, MailConfiguration>,
    old_mails: Arc<Vec<MailConfiguration>>,
    handles: Vec<thread::JoinHandle<()>>,
    default_mail: EmailAddress,
    mail_config: MailConfiguration
}

impl Server {
    pub fn new(
        mails: Vec<MailConfiguration>,
        default_mail_addr: EmailAddress,
        mail_config: MailConfiguration
    ) -> Self {
        let mut hmails = HashMap::new();

        for mail in mails.iter() {
            hmails.insert(mail.email.clone(), mail.clone());
        }

        Server {
            mails: hmails,
            handles: Vec::new(),
            old_mails: Arc::new(mails),
            default_mail: default_mail_addr,
            mail_config: mail_config
        }
    }

    pub fn run(
        mut self,
        mut raw_pipeline: StageManager,
        shell_rx: Receiver<String>,
        ) -> Result<(), Error> {
        info!("Selecting database");

        //let db_connection = self.pool.clone().get().unwrap();

        info!("Mail init started");

        let rx = self.setup_mail();

        info!("Mail init finished");

        let mut pipeline = Arc::new(Mutex::new(raw_pipeline));

        let force_next_turn = Arc::new(Mutex::new(false));
        let force_next_turn_cl_clone = force_next_turn.clone();

        //###SHELL COMMANDS - Start
        ::std::thread::spawn(move || loop {
            if let Ok(cmd) = shell_rx.recv() {
                println!("COMMAND {}", cmd);
                match cmd.trim() {
                    "turn" => {
                        let mut guard = force_next_turn_cl_clone.lock().unwrap();
                        *guard = true;
                    }
                    _ => error!("Cannot recognize command {}", cmd),
                }
            }
        });
        //###SHELL COMMANDS - End

        let pipeline_clone = pipeline.clone();
        //let db_connection_clone2 = db_connection.clone();
        let old_mails_clone = self.old_mails.clone();

        let mailbox = Arc::new(Mailbox::new(self.mail_config.clone()));
        let mailbox_clone = mailbox.clone();

        ::std::thread::spawn(move || {
            let client = Client::connect("localhost", 27017)
                .expect("Failed to initialize client.");
            let db_connection = MongoCollection(client.db("gottkanzler").collection("games"));

            loop {
                if let Ok(email) = rx.recv() {

                    match pipeline_clone
                        .lock()
                        .unwrap()
                        .run(&db_connection, email.clone(), &mailbox.clone())
                        {
                            Ok(answers) => {
                                for answer in answers {
                                    info!(
                                        "Sending reply to {}",
                                        answer.to
                                    );

                                    mailbox.reply(answer).unwrap();
                                }
                            }
                            Err(err) => {
                                let unchecked_config = self.mails.get(&self.default_mail);

                                if let Some(config) = unchecked_config {
                                    let old_subject = email.subject.clone();
                                    let mut answer = email
                                        .reply()
                                        .subject(&format!("FEHLER: {}", old_subject))
                                        .body(&format!("{}", err));

                                    mailbox.reply(answer).unwrap();
                                }
                            }
                        }
                }

                thread::sleep_ms(3000);
            }
        });

        let force_next_turn_clone = force_next_turn.clone();
        let mut already_triggered = false;

        let client_next_turn = MongoCollection(Client::connect("localhost", 27017)
                                               .expect("Failed to initialize client.").db("gottkanzler").collection("games"));

        loop {
            let time = Local::now();

            let hour = time.hour();
            let minute = time.minute();
            //let mut force_next_turn = false;

            let mut guard = force_next_turn_clone.lock().unwrap();
            if (hour == 23 && minute >= 55 && already_triggered == false) || *guard {
                info!("Starting next turn");
                already_triggered = true;
                *guard = false;
                drop(guard);

                if let Ok(mut pipeline) = pipeline.lock() {
                    //let db_connection = pool_clone_3.clone().get().unwrap();

                    pipeline.next_turn(&client_next_turn, &mailbox_clone.clone())
                        .unwrap();
                }
                else {
                    eprintln!("Poison error in pipelin");
                    continue;
                }

            } else if (hour != 23 && minute <= 55) {
                already_triggered = false;
            }

            thread::sleep_ms(1000);
        }

        Ok(())
    }

    /**
     * Setup the connections to receive emails. The method returns a Receiver, which
     * immediately sends the emails when they are received.
     * */
    fn setup_mail(&mut self) -> mpsc::Receiver<Email> {
        let (tx, rx) = mpsc::channel();

        debug!("Configuration contains {} mail addresses", self.mails.len());

        for config in self.mails.values() {
            let socket_addr = (config.domain.as_str(), config.port);
            let ssl_connector = TlsConnector::builder().build().unwrap();

            debug!("Connecting to {}:{}", &config.domain, &config.port);

            let mut imap_socket =
                imap::client::Client::secure_connect(socket_addr, &config.domain, &ssl_connector)
                .unwrap();

            debug!("Login to {}", &config.email);

            imap_socket.login(&config.email, &config.password).unwrap();

            let thread_tx = tx.clone();

            let email = config.email.clone();

            let handle = thread::spawn(move || {
                loop {
                    let emails = Mailbox::get_unseen_emails(&mut imap_socket);

                    debug!("Fetch unseen emails for {:?}", email);

                    if emails.len() > 0 {
                        for email in emails.into_iter() {
                            //if email.parent.is_none() {
                            info!("Got an email for {:?}", email);
                            thread_tx.send(email).unwrap();
                            //} else {
                            //debug!("Ignoring email, because it is the parent of a reply");
                            //}
                        }
                    }

                    thread::sleep_ms(3000);
                }
            });

            //handle.join().unwrap();
            self.handles.push(handle);
        }

        rx
    }
}
