use std::io::Read;
use std::fs::File;
use toml::from_str;

#[derive(Debug, Deserialize)]
pub struct Configuration {
    domain: String,
    email: Vec<MailConfiguration>,
    witai_token: String,
    pub database: DatabaseConfiguration
}

#[derive(Debug, Deserialize)]
pub struct DatabaseConfiguration {
    pub host: Option<String>,
    pub port: Option<u16>,
    pub auth: Option<DatabaseAuth>,
    pub database: String
}

#[derive(Debug, Deserialize)]
pub struct DatabaseAuth {
    pub username: String,
    pub password: String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct MailConfiguration {
    pub email: String,
    pub password: String,
    pub domain: String,
    pub port: u16,
    pub smtp_domain: String
}

impl Configuration {
    //FIXME Error type
    pub fn load_config(config_file: &str) -> Result<Self, ()> {
        let mut fs = File::open(config_file).expect(&format!("Cannot find {}", config_file));
        let mut content = String::new();

        fs.read_to_string(&mut content).unwrap();

        let config : Configuration = from_str(&content).unwrap();

        debug!("Configuration has {} mail accounts", config.email.len());

        Ok(config)
    }

    pub fn get_mails(&self) -> Vec<MailConfiguration> {
        self.email.clone()
    }

    pub fn get_witai_token(&self) -> String {
        self.witai_token.clone()
    }

    pub fn get_domain(&self) -> String {
        self.domain.clone()
    }

    pub fn get_addr(&self) -> String {
        format!("{}", self.domain.clone())
    }
}
