extern crate lettre_email;
extern crate lettre;
extern crate native_tls;
extern crate imap;
extern crate mailparse;
extern crate toml;
extern crate serde;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;
#[macro_use]
extern crate log;
extern crate env_logger;
#[macro_use]
extern crate failure;
#[macro_use]
extern crate failure_derive;
extern crate pcre;
// #[macro_use] extern crate diesel;
extern crate mongodb;
#[macro_use(bson, doc)]
extern crate bson;
extern crate rand;
extern crate reqwest;
extern crate chrono;
extern crate tempdir;

pub mod core;
pub mod server;
pub mod config;
pub mod mail;
pub mod witai;
pub mod db;
