use db::CollectionInterface;
use failure::Error;
use bson::Document;
use bson::oid::ObjectId;
use std::cell::RefCell;
use core::models::Game;
use std::collections::HashMap;
use bson;

pub struct DummyCollection {
    pub memory: RefCell<HashMap<ObjectId, Document>>
}

impl DummyCollection {
    pub fn new() -> Self  {
        DummyCollection {
            memory: RefCell::new(HashMap::new())
        }
    }
}

impl CollectionInterface for DummyCollection {
    fn update_one(&self, filter: Document, update: Document) -> Result<(), Error> {
        let mut game = self.find_one(filter.clone()).expect("no game not found");
        let mut game_copy = game.clone();

        let mut qu = match game_copy.get_mut("queue").unwrap() {
            bson::Bson::Array(d) => d,
            _=> panic!("")
        };

        let moves = match update.get("$push").unwrap() {
            bson::Bson::Document(d) => match d.get("queue").unwrap() {
                bson::Bson::Document(dd) => match dd.get("$each").unwrap() {
                    bson::Bson::Array(ddd) => ddd,
                    _ => panic!("")
                },
                _ => panic!("")

            },
            _ => panic!("")
        };

        for m in moves.iter() {
            qu.push(m.clone());
        }

        self.replace_one(filter, game);

        Ok(())
    }

    fn replace_one(&self, filter: Document, update: Document) -> Result<(), Error> {
        let id = filter.get_object_id("_id").unwrap();

        self.memory.borrow_mut().remove(id);
        //FIXME
        self.memory.borrow_mut().insert(id.clone(), update);

        Ok(())
    }

    fn find(&self, filter: Document) -> Vec<Document> {
        self.memory.borrow().values().cloned().collect::<Vec<Document>>()
    }

    /// Return document with the _id or return the first
    fn find_one(&self, filter: Document) -> Option<Document> {
        if let Ok(id) = filter.get_object_id("_id") {
            let memory = self.memory.borrow();

            let raw = memory.get(id).unwrap();

            Some(raw.clone())
        }
        else {
            Some(self.memory.borrow().values().cloned().take(1).collect::<Vec<Document>>()[0].clone())
        }
    }

    fn delete_one(&self, filter: Document) -> Result<(), Error> {
        let id = filter.get_object_id("_id").unwrap();

        self.memory.borrow_mut().remove(id);

        Ok(())
    }

    fn insert_one(&self, mut doc: Document) -> Result<ObjectId, Error> {
        let id = ObjectId::new().unwrap();

        doc.insert("_id", id.clone());

        let de_game = bson::from_bson(bson::Bson::Document(doc)).unwrap();

        self.memory.borrow_mut().insert(id.clone(), de_game);

        Ok(id)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn insert() {
        let coll = DummyCollection::new();
        coll.insert_one(doc!{});

        assert_eq!(coll.memory.borrow().len(), 1);
    }

    #[test]
    fn find() {
        let coll = DummyCollection::new();
        coll.insert_one(doc!{"test": "test"});

        assert_eq!(coll.memory.borrow().len(), 1);

        let docs = coll.find(doc!{});

        assert_eq!(docs.len(), 1);
    }

    #[test]
    fn find_one() {
        let coll = DummyCollection::new();
        let id = coll.insert_one(doc!{}).unwrap();

        let doc = coll.find_one(doc!{"_id": id.clone()});

        assert!(doc.is_some());


    }
}
