use bson;
use bson::Document;
pub use self::mongo::MongoCollection;
pub use self::dummy::DummyCollection;
use failure;

pub mod mongo;
pub(crate) mod dummy;

type Error = failure::Error;

pub trait CollectionInterface {
    fn update_one(&self, Document, Document) -> Result<(), Error>;
    fn replace_one(&self, Document, Document) -> Result<(), Error>;
    fn find_one(&self, Document) -> Option<Document>;
    fn find(&self, Document) -> Vec<Document>;
    fn delete_one(&self, Document) -> Result<(), Error>;
    fn insert_one(&self, Document) -> Result<bson::oid::ObjectId, Error>;

}
