use bson::oid::ObjectId;
use bson::Document;
use bson;
use db::CollectionInterface;
//use mongo_driver::client::Client as RawClient;
//use mongo_driver::cursor::Cursor;
//use mongo_driver::database::Database as RawDatabase;
//use mongo_driver::collection::Collection as RawCollection;
//use mongo_driver::collection::{CountOptions,FindAndModifyOperation};
use mongodb::{Client as RawClient, ThreadedClient};
use mongodb::db::ThreadedDatabase;
use mongodb::db::Database as RawDatabase;
use mongodb::coll::Collection as RawCollection;
use failure::{Error, err_msg};

pub struct MongoCollection(pub RawCollection);

impl CollectionInterface for MongoCollection{
    fn update_one(&self, filter: Document, update: Document) -> Result<(), Error> {
        match self.0.update_one(filter, update, None) {
            Err(err) => Err(err_msg(format!("Cannot update document {}", err))),
            Ok(_) => Ok(())
        }
    }

    fn replace_one(&self, filter: Document, update: Document) -> Result<(), Error> {
        match self.0.replace_one(filter, update, None) {
            Ok(_) => Ok(()),
            Err(err) =>  Err(err_msg(format!("Cannot replace document in the database {}", err)))
        }
    }

    fn find(&self, filter: Document) -> Vec<Document> {
        let res = self.0.find(Some(filter), None).unwrap();

        res.into_iter().map(|d| d.unwrap()).collect()
    }

    fn find_one(&self, filter: Document) -> Option<Document> {
        let res = self.0.find_one(Some(filter), None).unwrap();

        res

    }

    fn delete_one(&self, filter: Document) -> Result<(), Error> {
        match self.0.delete_one(filter, None) {
            Err(err) => Err(err_msg(format!("Cannot delete document in the database {}", err))),
            Ok(_) => Ok(())
        }
    }

    fn insert_one(&self, doc: Document) -> Result<ObjectId, Error> {
        match self.0.insert_one(doc, None) {
            Err(err) => Err(err_msg(format!("Cannot insert document in the database {}", err))),
            Ok(res) => {
                match res.inserted_id.unwrap() {
                    bson::Bson::ObjectId(id) => Ok(id),
                    _ => unreachable!("Wrong type")
                }
            }
        }
    }
}

