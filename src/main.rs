extern crate imap;
extern crate lettre;
extern crate lettre_email;
extern crate mailparse;
extern crate native_tls;
extern crate serde;
extern crate toml;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;
#[macro_use]
extern crate log;
extern crate env_logger;
#[macro_use]
extern crate failure;
#[macro_use]
extern crate failure_derive;
extern crate bson;
extern crate chrono;
extern crate mongodb;
extern crate pcre;
extern crate rand;
extern crate reqwest;
extern crate temp;
extern crate tempdir;

use std::os::unix::net::UnixListener;
use std::sync::mpsc;
use std::sync::mpsc::Receiver;
use std::thread;
use temp::config::Configuration;
use temp::core::pipeline::StageManager;
use temp::server::Server;
use tempdir::TempDir;

use mongodb::{Client, ThreadedClient};

fn main() {
    env_logger::init();
    let dir = TempDir::new("temp").unwrap();

    println!("Temp dir {:?}", dir.path());

    let loader =
        Configuration::load_config("configuration.toml").expect("Cannot find configuration file");

    info!("Configuration loaded");

    info!("Setup database");
    //let pool = setup_db(&loader).unwrap();

    info!("Setup test unix pipe");
    let test_unix_socket = setup_unix_socket(&loader, &dir).unwrap();

    let mails = loader.get_mails();
    let default = mails[0].email.clone();
    let default_config = mails[0].clone();

    let mut server = Server::new(mails, default, default_config);

    info!("Setup pipeline");
    let manager = setup_pipeline(&loader);

    server.run(manager, test_unix_socket).unwrap();
}

//TODO add to configuration
//fn setup_db(_loader: &Configuration) -> Result<Pool<MongodbConnectionManager>, ()> {
/*let manager = MongodbConnectionManager::new(
  ConnectionOptionsBuilder::new()
  .with_host("localhost")
  .with_port(27017)
  .with_db("gottkanzler")
//.with_username("root")
//.with_password("password")
.build(),
);

let pool = Pool::builder().max_size(4).build(manager).unwrap();

Ok(pool)
*/
//}

fn setup_pipeline(loader: &Configuration) -> StageManager {
    use temp::core::stages::intent::IntentStage;

    let mut manager = StageManager::new(loader.get_domain(), &loader.get_witai_token());

    manager
}

/**
 * This is returns a socket, which makes it possible to quickly test the turning feature
 */
fn setup_unix_socket(_loader: &Configuration, dir: &TempDir) -> Result<Receiver<String>, ()> {
    use std::io::{BufRead, BufReader};

    let listener =
        UnixListener::bind(dir.path().join("gottkanzler.sock")).expect("Cannot create unix socket");
    let (tx, rx) = mpsc::channel();

    thread::spawn(move || {
        for mut client in listener.incoming() {
            let tx2 = tx.clone();
            thread::spawn(move || {
                info!("New connection from shell");

                let client2 = client.unwrap();

                let mut reader = BufReader::new(&client2);
                let mut text = String::new();

                reader.read_line(&mut text).expect("Could not read");

                info!("Received from shell> {}", text.clone());

                tx2.send(text.to_string()).unwrap();
            });
        }
    });

    Ok(rx)
}
