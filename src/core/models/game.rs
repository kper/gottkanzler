use core::models::*;
use failure::{err_msg, Error};
use mail::MailAddress;
use db::CollectionInterface;

#[derive(Debug, Serialize, Deserialize)]
pub struct Game {
    #[serde(rename = "_id")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<GameId>,
    pub members: Vec<User>,
    //pub players: i32,
    pub map: Vec<District>,
    //pub moves: Vec<Move>, //Current
    pub queue: Vec<Move>, //Next
    pub game_over: bool,
    pub game_started: bool,
    pub admin: String,
}

impl Game {
    pub fn new(admin: String) -> Self {
        Game {
            id: None,
            members: Vec::new(),
            //  players: 5,
            map: Vec::new(),
            queue: Vec::new(),
            game_over: false,
            game_started: false,
            admin: admin,
        }
    }

    fn get_parties(&self) -> Vec<(String, Color)> {
        let mut parties = Vec::new();

        for i in self.members.iter() {
            parties.push((i.party.clone(), i.color.clone()));
        }

        parties.sort();

        parties
    }

    /**
     * Checks if the user already in the **this** game
     * Returns True - yes
     * */
    fn check_if_user_exists(&self, email: &str) -> bool {
        for user in self.members.iter() {
            if user.email == email {
                return true;
            }
        }

        false
    }

    /**
     * Checks if the user in **any** game
     * Returns True - yes
     *
     * */
    fn check_if_user_uniq(
        &self,
        db_connection: &impl CollectionInterface,
        email: &str,
        ) -> Result<bool, Error> {
        let filter = doc! {
            "members.email": email,
            "game_over": false,
        };

        //let cursor = db_connection.find(filter).map_err(|err| err_msg(format!("{}", err)))?;
        let cursor = db_connection.find(filter);

        let mut index = 0;

        for doc in cursor {
            index += 1;
        }

        match index {
            0 => Ok(false),
            _ => Ok(true)
        }
    }

    pub fn add_user(&mut self, db_connection: &impl CollectionInterface, email: String) -> Result<User, Error> {
        if self.game_started {
            return Err(err_msg("Das Spiel läuft bereits"));
        }
        if self.game_over {
            return Err(err_msg("Das Spiel ist bereits vorbei"));
        }
        if self.check_if_user_exists(&email) {
            bail!("User ist bereits in diesem Spiel")
        }
        if self.check_if_user_uniq(db_connection, &email)? {
            bail!("User ist bereits in einem anderen Spiel")
        }

        let mut party = String::new();
        let mut color = String::new();

        let mut copy_PARTIES = Vec::new();

        for i in PARTIES.iter() {
            copy_PARTIES.push(i.clone());
        }

        let mut already_given_parties = self.get_parties();

        if already_given_parties.len() == PARTIES.len() {
            return Err(err_msg("Too many players! Maximum achieved"));
        }

        thread_rng().shuffle(&mut copy_PARTIES);

        for (item_party, item_color) in copy_PARTIES.iter() {
            if !already_given_parties.contains(&(item_party.to_string(), item_color.to_string())) {
                party = item_party.clone().to_string();
                color = item_color.clone().to_string();
            }
        }

        Ok(User {
            email,
            party,
            color,
            figures: Vec::new(),
        })
    }

    pub fn update_moves(
        &self,
        db_connection: &impl CollectionInterface,
        game_moves: Vec<Move>,
        game: &Game,
        _user: &User,
        ) -> Result<(), Error> {
        debug!("Updating moves");

        let filter = doc! {
            "_id": game.id.clone().expect("Game has no game id")
        };

        let bson = bson::to_bson(&game_moves)
            .map_err(|err| err_msg("Cannot deserialize game").context(err))?;

        let update = doc! {
            "$push": {
                "queue": {
                    "$each": bson
                }
            }
        };

        db_connection
            .update_one(filter, update)?;
        //.map_err(|err| err_msg(format!("Cannot update game")).context(err))?;

        debug!("Updated!");

        Ok(())
    }

    fn find_user(&self, email: String) -> Result<usize, Error> {
        let mut index = 0 as usize;
        for user in self.members.iter() {
            if user.email == email {
                return Ok(index);
            } else {
                index += 1;
            }
        }

        bail!("User {} was not found", email)
    }

    pub fn find_district(&self, district: &str) -> Result<&District, Error> {
        for d in self.map.iter() {
            if d.name == district {
                return Ok(d);
            }
        }

        bail!("No district found in self.map {}", district);
    }

    pub fn find_district_mut(&mut self, district: &str) -> Result<&mut District, Error> {
        for d in self.map.iter_mut() {
            if d.name == district {
                return Ok(d);
            }
        }

        bail!("No district found in self.map {}", district);
    }

    pub fn update_party_presence(&mut self) -> Result<(), Error> {
        debug!("update_party_presence");
        let players_len = self.members.len() as i32;
        for user in self.members.clone().iter() {
            for pos in user.figures.iter() {
                let mut district = self.find_district_mut(&pos.district)?;
                district.shrink_presence_except(&user.party, players_len)?;
            }
        }

        Ok(())
    }

    /// Returns the current name of the district where the `next_move` figure is. This
    /// method requires that the index of `self.members` is already known.
    fn find_current_district_str_by_figure(&self, user_index: usize, next_move: &Move) -> String {
        for pos in self.members[user_index].figures.iter() {
            if next_move.position.figure == pos.figure {
                return pos.district.clone();
            }
        }

        String::new()
    }

    /// Returns the current district of the user's figure.
    fn find_current_district_by_figure(&self, user_index: usize, next_move: &Move) -> Result<&District, Error> {
        let current_district_str = self.find_current_district_str_by_figure(user_index, next_move);

        self.find_district(&current_district_str)
    }

    /// Check if the `next_position.district` is a direct neighbour of `current_district`
    fn check_if_neighbour(&self, current_district: &District, next_position: &Position) -> bool {
        let mut found = false;
        for neighbour in current_district.neighbours.iter() {
            if *neighbour == next_position.district {
                found = true;
                break;
            }
        }

        found
    }

    /// Check if the next_position's district exists
    fn check_if_next_district_exists(&self, next_position: &Position) -> bool {
        let districts : Vec<_> = self.map.iter().map(|d| &d.name).collect();

        districts.contains(&&next_position.district)
    }

    fn get_mut_position_by_figure(&mut self, user_index: usize, figure: &Figure) -> Option<&mut Position> {
        for mut pos in self.members[user_index].figures.iter_mut() {
            if pos.figure == *figure {
                //pos.district = next_position.district.clone();
                return Some(pos);
            }
        }

        None
    }

    pub fn apply_queue(&mut self, domain: &str) -> Result<Vec<(String, String)>, Error> {
        debug!("apply_queue");

        let mut failed = Vec::new();

        for game_move in self.queue.clone().into_iter() {
            let user_index = self.find_user(game_move.email.clone())?;
            let next_position = &game_move.position;

            let current_district = self.find_current_district_by_figure(user_index, &game_move)?.clone();

            if !self.check_if_next_district_exists(next_position) {
                let error = format!(
                    "Der Zug ist fehlgeschlagen, weil {} nicht erkannt wurde! \n\n {}/game/{}",
                    next_position.district,
                    domain,
                    self.id.clone().unwrap());

                failed.push((game_move.email.clone(), error));

                break;
            }

            if self.check_if_neighbour(&current_district, next_position) {
                // Update the position - SUPER IMPORTANT
                let mut copy_old_position = None;
                if let Some(mut pos) = self.get_mut_position_by_figure(user_index, &next_position.figure) {
                    copy_old_position = Some(pos.clone());
                    pos.district = next_position.district.clone();
                };
                // SUPER IMPORTANT - END

                //Save it to reference_moves for sending reports
                let str_district = &game_move.position.district;
                let district = self.find_district_mut(str_district)?;

                let mut reference_game_move = game_move.clone();
                reference_game_move.old = copy_old_position;

                district.reference_moves.push(reference_game_move);
            } else {
                let error = format!(
                    "Der Zug ist fehlgeschlagen, weil es nicht möglich ist von {} nach {} die Figur zu bewegen. Beide Felder liegen nicht nebeneinander. \n\n {}/game/{}",
                    current_district.name, next_position.district, domain, self.id.clone().unwrap());

                failed.push((game_move.email.clone(), error));
            }
        }

        Ok(failed)
    }

    /// Resolves combats where both players attacks the same field.
    pub fn resolve_combats_same_attacking_district(&mut self, successful_moves: &mut Vec<Move>, rejected_moves: &mut Vec<Move>) -> Result<(), Error> {
        type Email = String;
        type CombatPower = i32;

        let map = self.map.iter_mut().filter(|d| d.reference_moves.len() > 0);

        for district in map {
            debug!("Resolving conflict in {}", district.name);

            //Count combat forces
            let mut counter: HashMap<Email, CombatPower> = HashMap::new();

            for game_move in district.reference_moves.iter() {
                if counter.contains_key(&game_move.email) {
                    let mut value = counter.get_mut(&game_move.email).ok_or_else(|| {
                        err_msg(format!("Did not find email {}", &game_move.email))
                    })?;
                    *value += 1;
                } else {
                    counter.insert(game_move.email.clone(), 1);
                }
            }

            if counter.len() == 1 {
                successful_moves.push(district.reference_moves[0].clone());
            } else if counter.len() > 1 {
                //Multiple parties on a field

                let max: CombatPower = counter.values().cloned().max().unwrap();

                let winners: Vec<(&Email, &CombatPower)> = counter
                    .iter()
                    .filter(|(_email, combat_power)| **combat_power == max)
                    .collect();
                let losers: Vec<(&Email, &CombatPower)> = counter
                    .iter()
                    .filter(|(_email, combat_power)| **combat_power < max)
                    .collect();

                if winners.len() == 0 {
                    unreachable!("This should not happen");
                } else if winners.len() == 1 {
                    let mut game_moves: Vec<Move> = district
                        .reference_moves
                        .clone()
                        .into_iter()
                        .filter(|g| g.email == *winners[0].0)
                        .collect();

                    successful_moves.append(&mut game_moves);
                } else if winners.len() > 1 {
                    for (email, _) in winners.iter() {
                        let mut game_moves: Vec<Move> = district
                            .reference_moves
                            .clone()
                            .into_iter()
                            .filter(|g| g.email == **email)
                            .collect();

                        rejected_moves.append(&mut game_moves);
                    }
                }

                for (email, _) in losers.iter() {
                    let mut game_moves: Vec<Move> = district
                        .reference_moves
                        .clone()
                        .into_iter()
                        .filter(|g| g.email == **email)
                        .collect();

                    rejected_moves.append(&mut game_moves);
                }

            }

            district.reference_moves.clear();
        }

        Ok(())
    }

    /// Resolves combats where players attack a field where already one figure is there
    pub fn resolve_combats_same_attacking_district_where_figures_already_there(&mut self, successful_moves: &mut Vec<Move>, rejected_moves: &mut Vec<Move>) -> Result<(), Error> {
        type Email = String;
        type CombatPower = i32;

        let mut positions = Vec::new();

        for figures in self.members.iter().map(|m| m.figures.clone()).collect::<Vec<_>>() {
            for figure in figures.into_iter() {
                positions.push(figure);
            }
        }

        let map = self.map.iter_mut().filter(|d| d.reference_moves.len() > 0).filter(|d| {
            let district_name = d.name.clone();
            //Get all districts where figures are on it
            return positions.iter().filter(|p| p.district == district_name).collect::<Vec<_>>().len() > 0;
        });

        for mut district in map {
            debug!("Resolving conflict in {}", district.name);

            //Count combat forces
            let mut counter: HashMap<Email, CombatPower> = HashMap::new();

            //Count combat power of different users
            for game_move in district.reference_moves.iter() {
                if counter.contains_key(&game_move.email) {
                    let mut value = counter.get_mut(&game_move.email).ok_or_else(|| {
                        err_msg(format!("Did not find email {}", &game_move.email))
                    })?;
                    *value += 1;
                } else {
                    counter.insert(game_move.email.clone(), 1);
                }
            }

            if counter.len() > 0 {
                //It fails, because one player is already there
                for m in district.reference_moves.iter() {
                    rejected_moves.push(m.clone());
                }

                district.reference_moves.clear();
            } else {
                unreachable!("This should happen");
            }

        }

        Ok(())
    }

    pub fn resolve_combats(
        &mut self,
        successful_moves: &mut Vec<Move>,
        rejected_moves: &mut Vec<Move>,
        ) -> Result<(), Error> {

        self.resolve_combats_same_attacking_district(successful_moves, rejected_moves)?;

        //Rollback

        for rej in rejected_moves.iter().cloned() {
            self.rollback(rej)?;
        }

        self.resolve_combats_same_attacking_district_where_figures_already_there(successful_moves, rejected_moves)?;
        //Rollback

        for rej in rejected_moves.iter().cloned() {
            self.rollback(rej)?;
        }

        self.queue.clear();
        Ok(())
    }

    pub fn rollback(&mut self, reference_move: Move) -> Result<(), Error> {
        let index = self.find_user(reference_move.email.clone()).unwrap().clone();
        let mut user = &mut self.members[index];
        user.rollback_move(reference_move)
    }

    pub fn start(&mut self) -> Result<(), Error> {
        if self.game_started {
            error!("Game was already started ({})", self.id.clone().unwrap());
            return Err(err_msg("Das Spiel wurde bereits gestartet :)"));
        }
        if self.game_over {
            error!("Game is already over ({})", self.id.clone().unwrap());
            return Err(err_msg("Das Spiel ist vorbei! Starte doch ein Neues"));
        }

        info!("Game has started!");
        let players = self.members.len();
        let parties = self.get_parties();

        self.game_started = true;

        self.setup_districts();

        assert_eq!(self.map.len(), 94);

        // Main Areas
        for i in 0..players {
            let user = &mut self.members[i as usize];

            //Party Presence
            for i in 0..(94 / players){
                loop {
                    let mut r: u32 = 100;
                    let d: u32 = random::<u32>() % 94;
                    let mut district = &mut self.map[d as usize];

                    if district.values.len() > 0 {
                        continue;
                    }

                    district.values.push(PartyPresence {
                        name: user.party.clone(),
                        value: r as i32,
                        color: user.color.clone(),
                    });

                    //Figures
                    if i == 0 {
                        user.figures.push(Position {
                            district: district.name.clone(),
                            figure: Figure::Reiter,
                            color: user.color.clone(),
                            party: user.party.clone(),
                        });
                    }

                    if i == 1 {
                        user.figures.push(Position {
                            district: district.name.clone(),
                            figure: Figure::Turm,
                            color: user.color.clone(),
                            party: user.party.clone(),
                        });
                    }

                    break;
                }
            }
        }

        // Remainder Presence
        for i in 0..94 {
            let mut district = &mut self.map[i as usize];

            fn contains_key(vector: &Vec<PartyPresence>, key: &str) -> bool {
                for k in vector.iter() {
                    if k.name == key {
                        return true;
                    }
                }

                false
            }

            for (party, color) in PARTIES.iter() {
                if !contains_key(&district.values, party) {
                    district.values.push(PartyPresence {
                        name: party.to_string(),
                        color: color.to_string(),
                        value: 0,
                    });
                }
            }
        }

        Ok(())
    }

    pub fn end(&mut self) {
        info!("A game just finished!");
        self.game_over = true;
    }

    pub fn replace(&self, db_connection: &impl CollectionInterface) -> Result<(), Error> {
        let filter = doc! {
            "_id": (self.id.clone().unwrap())
        };

        let ser_game =
            bson::to_bson(&self).map_err(|_| err_msg(format!("Cannot parse game to bson")))?;

        if let bson::Bson::Document(document) = ser_game {
            db_connection
                .replace_one(filter, document).unwrap();
            //.map_err(|_| err_msg(format!("Cannot replace document in the database")))?;
        } else {
            return Err(err_msg(format!("Serialized document was not a document")));
        }

        Ok(())
    }

    pub fn get_scores(&self) -> Vec<Score> {
        let mut counter: HashMap<(String, Color), i32> = HashMap::new();
        let mut total = 0 as i32;

        for district in self.map.iter() {
            for party_presence in district.values.iter() {
                if counter
                    .contains_key(&(party_presence.name.clone(), party_presence.color.clone()))
                    {
                        let mut val = counter
                            .get_mut(&(party_presence.name.clone(), party_presence.color.clone()))
                            .unwrap();
                        *val += party_presence.value;
                    } else {
                        counter.insert(
                            (party_presence.name.clone(), party_presence.color.clone()),
                            party_presence.value,
                            );
                    }

                total += party_presence.value;
            }
        }

        counter
            .iter()
            .map(|((key, color), value)| Score {
                party: key.clone(),
                value: *value,
                value_p: format!("{:.2}", (*value as f64 / total as f64) * 100 as f64),
                color: color.clone(),
            })
        .collect()
    }

    /**
     * Returns the winner
     * */
    pub fn check_end(&self) -> Option<String> {
        let scores = self.get_scores();

        let winner = scores
            .iter()
            .filter(|score| (score.value_p.parse::<f64>().unwrap()) > 50f64)
            .collect::<Vec<&Score>>();

        /*
           let mut winner = Vec::new();

           for w in scores.iter() {
           println!("{}", w.value_p);
           if w.value_p > 0.5f64 {
           println!("{}", w.value_p);
           winner.push(w);
           }
           }
           */

        match winner.len() {
            0 => None,
            _ => Some(winner[0].party.clone()),
        }
    }

    pub fn sort_map(&mut self) {
        let parties: Vec<&str> = PARTIES.iter().map(|p| p.0.clone()).collect();

        fn find_index_by_name(values: &Vec<PartyPresence>, key: &str) -> Option<usize> {
            let mut index = 0;
            for i in values.iter() {
                if i.name == key {
                    return Some(index);
                }

                index += 1;
            }

            None
        }

        for district in &mut self.map {
            let mut i = 0;
            for pp in district.values.clone().iter() {
                if parties[i] != pp.name {
                    let index = find_index_by_name(&district.values, parties[i]);
                    district.values.swap(i, index.unwrap());
                }

                i += 1;
            }
        }
    }

    pub fn player_surrenders(&mut self, player: MailAddress) -> Result<(), Error> {
        let index = self.find_user(player.get_inner().to_string())?;
        let party = self.members[index].party.clone();

        self.members.remove(index);

        for district in self.map.iter_mut() {
            let dindex = district.find_party_in_values(party.clone());
            district.values.remove(dindex);

            let moves: Vec<usize> = district
                .find_move_in_values(player.get_inner().to_string())
                .iter()
                .map(|&(index, _)| index)
                .collect();

            for index in moves.into_iter() {
                district.reference_moves.remove(index);
            }
        }

        Ok(())
    }

    pub fn make_move(&mut self, email: &MailAddress, game_move: &Move) -> Result<(), Error> {
        let figure = game_move.position.figure.clone();

        let ind: Vec<usize> = self
            .queue
            .iter()
            .enumerate()
            .filter(|&(index, m)| m.email == email.get_inner() && m.position.figure == figure)
            .map(|(index, _)| index)
            .collect();

        if ind.len() == 0 {
            self.queue.push(game_move.clone());
        } else {
            self.queue.remove(ind[0]);
            self.queue.push(game_move.clone());
        }

        Ok(())
    }

    fn create_district<'a>(
        map: &'a mut Vec<District>,
        name: &'a str,
        neighbours: Vec<&str>,
        ) -> &'a mut Vec<District> {
        map.push(District {
            name: name.to_string(),
            values: Vec::new(),
            reference_moves: Vec::new(),
            neighbours: neighbours.iter().map(|s| s.to_string()).collect(),
        });

        map
    }

    fn setup_districts(&mut self) {
        let mut map = Vec::new();

        Game::create_district(
            &mut map,
            "Bludenz",
            vec!["Feldkirch", "Bregenz", "Reutte", "Landeck"],
            );
        Game::create_district(
            &mut map,
            "Wels-Land",
            vec![
            "Wels(Stadt)",
            "Grieskirchen",
            "Vöcklabruck",
            "Gmunden",
            "Kirchdorf an der Krems",
            "Steyr Land",
            "Linz-Land",
            "Eferding",
            ],
            );
        Game::create_district(
            &mut map,
            "Salzburg-Umgebung",
            vec![
            "Salzburg(Stadt)",
            "Braunau am Inn",
            "Vöcklabruck",
            "Gmunden",
            "Hallein",
            ],
            );
        Game::create_district(
            &mut map,
            "Wolfsberg",
            vec![
            "Sankt Veit an der Glan",
            "Murtal",
            "Voitsberg",
            "Deutschlandsberg",
            "Völkermarkt",
            ],
            );
        Game::create_district(
            &mut map,
            "Korneuburg",
            vec!["Hollabrunn", "Mistelbach", "Wien(Stadt)", "Tulln"],
            );
        Game::create_district(
            &mut map,
            "Freistadt",
            vec!["Urfahr-Umgebung", "Gmünd", "Zwettl", "Perg"],
            );
        Game::create_district(
            &mut map,
            "Wiener Neustadt(Stadt)",
            vec!["Wiener Neustadt(Land)", "Mattersburg", "Neunkirchen"],
            );
        Game::create_district(
            &mut map,
            "Linz-Land",
            vec![
            "Linz(Stadt)",
            "Urfahr-Umgebung",
            "Wels-Land",
            "Steyr-Land",
            "Amstetten",
            "Perg",
            "Eferding",
            ],
            );
        Game::create_district(
            &mut map,
            "Klagenfurt Stadt",
            vec!["Klagenfurt Land", "Sankt Veit an der Glan", "Feldkirchen"],
            );
        Game::create_district(
            &mut map,
            "Zwettl",
            vec![
            "Waidhofen an der Thaya",
            "Gmünd",
            "Freistadt",
            "Perg",
            "Melk",
            "Krems(Land)",
            "Horn",
            ],
            );
        Game::create_district(
            &mut map,
            "Neusiedl am See",
            vec!["Bruck an der Leitha", "Eisenstadt-Umgebung", "Rust(Stadt)"],
            );
        Game::create_district(
            &mut map,
            "Murtal",
            vec![
            "Liezen",
            "Leoben",
            "Graz-Umgebung",
            "Voitsberg",
            "Wolfsberg",
            "Sankt Veit an der Glan",
            "Murau",
            ],
            );
        Game::create_district(
            &mut map,
            "Melk",
            vec![
            "Krems(Land)",
            "Zwettl",
            "Perg",
            "Amstetten",
            "Scheibbs",
            "Sankt Pölten(Land)",
            "Krems(Land)",
            ],
            );
        Game::create_district(
            &mut map,
            "Liezen",
            vec![
            "Kirchdorf an der Krems",
            "Gmunden",
            "Sankt Johann im Pongau",
            "Tamsweg",
            "Murau",
            "Murtal",
            "Leoben",
            "Bruck-Mürzzuschlag",
            "Scheibbs",
            "Amstetten",
            "Steyr-Land",
            ],
            );
        Game::create_district(
            &mut map,
            "Völkermarkt",
            vec!["Klagenfurt Land", "Sankt Veit an der Glan", "Wolfsberg"],
            );
        Game::create_district(
            &mut map,
            "Horn",
            vec![
            "Waidhofen an der Thaya",
            "Zwettl",
            "Krems(Land)",
            "Hollabrunn",
            ],
            );
        Game::create_district(
            &mut map,
            "Waidhofen an der Thaya",
            vec!["Horn", "Zwettl", "Gmünd"],
            );
        Game::create_district(&mut map, "Kufstein", vec!["Schwaz", "Kitzbühel"]);
        Game::create_district(
            &mut map,
            "Eferding",
            vec![
            "Rohrbach",
            "Urfahr-Umgebung",
            "Linz-Land",
            "Wels-Land",
            "Grieskirchen",
            "Schärding",
            ],
            );
        Game::create_district(
            &mut map,
            "Amstetten",
            vec![
            "Perg",
            "Melk",
            "Scheibbs",
            "Waidhofen an der Ybbs",
            "Steyr-Land",
            "Steyr(Stadt)",
            "Linz-Land",
            "Liezen",
            ],
            );
        Game::create_district(
            &mut map,
            "Perg",
            vec![
            "Amstetten",
            "Freistadt",
            "Urfahr-Umgebung",
            "Linz(Stadt)",
            "Linz-Land",
            "Melk",
            "Zwettl",
            ],
            );
        Game::create_district(
            &mut map,
            "Sankt Pölten(Land)",
            vec![
            "Sankt Pölten(Stadt)",
            "Tulln",
            "Wien(Stadt)",
            "Mödling",
            "Baden",
            "Lilienfeld",
            "Melk",
            "Krems-Land",
            "Krems an der Donau(Stadt)",
            ],
            );
        Game::create_district(&mut map, "Innsbruck-Stadt", vec!["Innsbruck-Land"]);
        Game::create_district(
            &mut map,
            "Güssing",
            vec!["Oberwart", "Hartberg-Fürstenfeld", "Südoststeiermark"],
            );
        Game::create_district(
            &mut map,
            "Deutschlandsberg",
            vec!["Voitsberg", "Graz-Umgebung", "Leibnitz", "Wolfsberg"],
            );
        Game::create_district(&mut map, "Salzburg(Stadt)", vec!["Salzburg-Umgebung"]);
        Game::create_district(
            &mut map,
            "Leibnitz",
            vec!["Graz-Umgebung", "Deutschlandsberg", "Südoststeiermark"],
            );
        Game::create_district(
            &mut map,
            "Lienz",
            vec!["Spittal an der Drau", "Hermagor", "Zell am See"],
            );
        Game::create_district(
            &mut map,
            "Hollabrunn",
            vec!["Horn", "Krems(Land)", "Tulln", "Korneuburg", "Mistelbach"],
            );
        Game::create_district(
            &mut map,
            "Wiener Neustadt(Land)",
            vec![
            "Wiener Neustadt(Stadt)",
            "Neunkirchen",
            "Baden",
            "Eisenstadt-Umgebung",
            "Mattersburg",
            "Oberpullendorf",
            "Oberwart",
            "Hartberg-Fürstenfeld",
            ],
            );
        Game::create_district(
            &mut map,
            "Braunau am Inn",
            vec!["Ried im Innkreis", "Vöcklbruck", "Salzburg-Umgebung"],
            );
        Game::create_district(
            &mut map,
            "Baden",
            vec![
            "Mödling",
            "Bruck an der Leitha",
            "Eisenstadt-Umgebung",
            "Wiener Neustadt(Land)",
            "Lilienfeld",
            "Sankt Pölten(Land)",
            ],
            );
        Game::create_district(
            &mut map,
            "Ried im Innkreis",
            vec![
            "Braunau am Inn",
            "Schärding",
            "Grieskirchen",
            "Vöcklabruck",
            ],
            );
        Game::create_district(
            &mut map,
            "Hermagor",
            vec!["Spittal an der Drau", "Villach Land", "Lienz"],
            );
        Game::create_district(
            &mut map,
            "Neunkirchen",
            vec![
            "Wiener Neustadt(Land)",
            "Wiener Neustadt(Stadt)",
            "Hartberg-Fürstenfeld",
            "Weiz",
            "Bruck-Mürzzuschlag",
            "Lilienfeld",
            ],
            );
        Game::create_district(
            &mut map,
            "Jennersdorf",
            vec!["Hartberg-Fürstenfeld", "Güssing", "Südoststeiermark"],
            );
        Game::create_district(
            &mut map,
            "Zell am See",
            vec![
            "Lienz",
            "Spittal an der Drau",
            "Sankt Johann im Pongau",
            "Kitzbühel",
            "Schwaz",
            ],
            );
        Game::create_district(
            &mut map,
            "Krems(Land)",
            vec![
            "Horn",
            "Zwettl",
            "Melk",
            "Sankt Pölten(Land)",
            "Krems an der Donau(Stadt)",
            "Tulln",
            "Hollabrunn",
            ],
            );
        Game::create_district(
            &mut map,
            "Leoben",
            vec!["Murtal", "Liezen", "Bruck-Mürzzuschlag", "Graz-Umgebung"],
            );
        Game::create_district(
            &mut map,
            "Mödling",
            vec![
            "Sankt Pölten(Land)",
            "Wien(Stadt)",
            "Bruck an der Leitha",
            "Baden",
            ],
            );
        Game::create_district(
            &mut map,
            "Scheibbs",
            vec![
            "Amstetten",
            "Waidhofen an der Ybbs",
            "Melk",
            "Sankt Pölten(Land)",
            "Lilienfeld",
            "Bruck-Mürzzuschlag",
            "Liezen",
            ],
            );
        Game::create_district(
            &mut map,
            "Rust(Stadt)",
            vec!["Eisenstadt-Umgebung", "Neusiedl am See"],
            );
        Game::create_district(&mut map, "Wels(Stadt)", vec!["Wels Land"]);
        Game::create_district(
            &mut map,
            "Bruck an der Leitha",
            vec![
            "Gänserndorf",
            "Wien(Stadt)",
            "Mödling",
            "Baden",
            "Eisenstadt-Umgebung",
            "Neusiedl am See",
            ],
            );
        Game::create_district(
            &mut map,
            "Rohrbach",
            vec!["Eferding", "Grieskirchen", "Schärding", "Urfahr-Umgebung"],
            );
        Game::create_district(
            &mut map,
            "Villach Stadt",
            vec!["Villach Land", "Feldkirchen"],
            );
        Game::create_district(
            &mut map,
            "Reutte",
            vec!["Imst", "Landeck", "Bludenz", "Bregenz"],
            );
        Game::create_district(
            &mut map,
            "Schärding",
            vec!["Ried im Innkreis", "Grieskirchen", "Rohrbach"],
            );
        Game::create_district(
            &mut map,
            "Voitsberg",
            vec!["Murtal", "Graz-Umgebung", "Deutschlandsberg", "Wolfsberg"],
            );
        Game::create_district(
            &mut map,
            "Mistelbach",
            vec!["Gänserndorf", "Korneuburg", "Hollabrunn"],
            );
        Game::create_district(
            &mut map,
            "Tulln",
            vec![
            "Hollabrunn",
            "Korneuburg",
            "Wien(Stadt)",
            "Sankt Pölten(Land)",
            "Krems an der Donau(Stadt)",
            "Krems(Land)",
            ],
            );
        Game::create_district(
            &mut map,
            "Eisenstadt-Umgebung",
            vec![
            "Eisenstadt(Stadt)",
            "Bruck an der Leitha",
            "Neusiedl am See",
            "Rust(Stadt)",
            "Mattersburg",
            "Wiener Neustadt(Land)",
            "Baden",
            ],
            );
        Game::create_district(&mut map, "Steyr(Stadt)", vec!["Amstetten", "Steyr-Land"]);
        Game::create_district(
            &mut map,
            "Sankt Veit an der Glan",
            vec![
            "Feldkirchen",
            "Murau",
            "Wolfsberg",
            "Völkermarkt",
            "Klagenfurt Land",
            "Klagenfurt Stadt",
            "Murtal",
            ],
            );
        Game::create_district(
            &mut map,
            "Gänserndorf",
            vec![
            "Mistelbach",
            "Korneuburg",
            "Wien(Stadt)",
            "Bruck an der Leitha",
            ],
            );
        Game::create_district(
            &mut map,
            "Schwaz",
            vec!["Innsbruck-Land", "Kufstein", "Kitzbühel", "Zell am See"],
            );
        Game::create_district(
            &mut map,
            "Sankt Johann im Pongau",
            vec![
            "Zell am See",
            "Hallein",
            "Gmunden",
            "Liezen",
            "Tamsweg",
            "Spittal an der Drau",
            ],
            );
        Game::create_district(
            &mut map,
            "Wien(Stadt)",
            vec![
            "Korneuburg",
            "Gänserndorf",
            "Bruck an der Leitha",
            "Mödling",
            "Sankt Pölten(Land)",
            "Tulln",
            "Mistelbach",
            ],
            );
        Game::create_district(
            &mut map,
            "Kirchdorf an der Krems",
            vec!["Steyr-Land", "Wels-Land", "Linz-Land", "Gmunden"],
            );
        Game::create_district(
            &mut map,
            "Krems an der Donau(Stadt)",
            vec!["Krems(Land)", "Sankt Pölten(Land)"],
            );
        Game::create_district(
            &mut map,
            "Feldkirchen",
            vec![
            "Villach Land",
            "Spittal an der Drau",
            "Murau",
            "Sankt Veit an der Glan",
            "Klagenfurt Land",
            "Klagenfurt Stadt",
            "Villach Stadt",
            ],
            );
        Game::create_district(
            &mut map,
            "Steyr-Land",
            vec![
            "Linz-Land",
            "Steyr(Stadt)",
            "Amstetten",
            "Waidhofen an der Ybbs(Stadt)",
            "Liezen",
            "Kirchdorf an der Krems",
            "Wels-Land",
            ],
            );
        Game::create_district(
            &mut map,
            "Weiz",
            vec![
            "Bruck-Mürzzuschlag",
            "Neunkirchen",
            "Hartberg-Fürstenfeld",
            "Südoststeiermark",
            "Graz-Umgebung",
            ],
            );
        Game::create_district(
            &mut map,
            "Imst",
            vec!["Landeck", "Reutte", "Innsbruck-Land"],
            );
        Game::create_district(
            &mut map,
            "Südoststeiermark",
            vec![
            "Leibnitz",
            "Graz-Umgebung",
            "Weiz",
            "Hartberg-Fürstenfeld",
            "Güssing",
            ],
            );
        Game::create_district(
            &mut map,
            "Linz(Stadt)",
            vec!["Urfahr-Umgebung", "Perg", "Linz-Land"],
            );
        Game::create_district(
            &mut map,
            "Gmünd",
            vec!["Waidhofen an der Thaya", "Zwettl", "Freistadt"],
            );
        Game::create_district(
            &mut map,
            "Mattersburg",
            vec![
            "Eisenstadt-Umgebung",
            "Wiener Neustadt(Stadt)",
            "Wiener Neustadt(Land)",
            "Oberpullendorf",
            ],
            );
        Game::create_district(
            &mut map,
            "Klagenfurt Land",
            vec![
            "Villach Land",
            "Feldkirchen",
            "Sankt Veit an der Glan",
            "Völkermarkt",
            ],
            );
        Game::create_district(
            &mut map,
            "Oberpullendorf",
            vec!["Mattersburg", "Wiener Neustadt(Land)", "Oberwart"],
            );
        Game::create_district(&mut map, "Dornbirn", vec!["Bregenz", "Feldkirch"]);
        Game::create_district(
            &mut map,
            "Tamsweg",
            vec![
            "Liezen",
            "Sankt Johann im Pongau",
            "Spittal an der Drau",
            "Murau",
            ],
            );
        Game::create_district(
            &mut map,
            "Grieskirchen",
            vec![
            "Eferding",
            "Schärding",
            "Ried im Innkreis",
            "Vöcklabruck",
            "Wels-Land",
            ],
            );
        Game::create_district(
            &mut map,
            "Sankt Pölten(Stadt)",
            vec!["Sankt Pölten(Land)"],
            );
        Game::create_district(
            &mut map,
            "Urfahr-Umgebung",
            vec![
            "Rohrbach",
            "Freistadt",
            "Linz(Stadt)",
            "Linz-Land",
            "Eferding",
            ],
            );
        Game::create_district(
            &mut map,
            "Murau",
            vec![
            "Tamsweg",
            "Liezen",
            "Murtal",
            "Sankt Veit an der Glan",
            "Feldkirchen",
            "Spittal an der Drau",
            ],
            );
        Game::create_district(
            &mut map,
            "Innsbruck-Land",
            vec!["Innsbruck-Stadt", "Schwaz", "Imst"],
            );
        Game::create_district(&mut map, "Eisenstadt(Stadt)", vec!["Eisenstadt-Umgebung"]);
        Game::create_district(
            &mut map,
            "Hartberg-Fürstenfeld",
            vec![
            "Oberwart",
            "Güssing",
            "Südoststeiermark",
            "Weiz",
            "Neunkirchen",
            "Wiener Neustadt(Land)",
            ],
            );
        Game::create_district(
            &mut map,
            "Graz-Umgebung",
            vec![
            "Bruck-Mürzzuschlag",
            "Weiz",
            "Graz(Stadt)",
            "Voitsberg",
            "Deutschlandsberg",
            "Leibnitz",
            "Südoststeiermark",
            "Leoben",
            ],
            );
        Game::create_district(
            &mut map,
            "Villach Land",
            vec![
            "Spittal an der Drau",
            "Villach Stadt",
            "Feldkirch",
            "Klagenfurt Land",
            "Hermagor",
            ],
            );
        Game::create_district(
            &mut map,
            "Oberwart",
            vec![
            "Güssing",
            "Hartberg-Fürstenfeld",
            "Wiener Neustadt(Land)",
            "Oberpullendorf",
            ],
            );
        Game::create_district(
            &mut map,
            "Bregenz",
            vec!["Dornbirn", "Bludenz", "Reutte", "Feldkirch"],
            );
        Game::create_district(
            &mut map,
            "Lilienfeld",
            vec![
            "Sankt Pölten(Land)",
            "Baden",
            "Wiener Neustadt(Land)",
            "Neunkirchen",
            "Bruck-Mürzzuschlag",
            "Scheibbs",
            ],
            );
        Game::create_district(
            &mut map,
            "Hallein",
            vec!["Salzburg-Umgebung", "Gmunden", "Sankt Johann im Pongau"],
            );
        Game::create_district(&mut map, "Kitzbühel", vec!["Kufstein", "Zell am See"]);
        Game::create_district(
            &mut map,
            "Gmunden",
            vec![
            "Vöcklabruck",
            "Kirchdorf an der Krems",
            "Liezen",
            "Sankt Johann im Pongau",
            "Hallein",
            "Salzburg-Umgebung",
            "Wels-Land",
            ],
            );
        Game::create_district(
            &mut map,
            "Waidhofen an der Ybbs(Stadt)",
            vec!["Amstetten", "Scheibbs", "Steyr-Land"],
            );
        Game::create_district(
            &mut map,
            "Vöcklabruck",
            vec![
            "Ried im Innkreis",
            "Braunau am Inn",
            "Salzburg-Umgebung",
            "Gmunden",
            "Wels-Land",
            "Grieskirchen",
            ],
            );
        Game::create_district(&mut map, "Landeck", vec!["Imst", "Reutte", "Bludenz"]);
        Game::create_district(
            &mut map,
            "Bruck-Mürzzuschlag",
            vec![
            "Scheibbs",
            "Lilienfeld",
            "Neunkirchen",
            "Weiz",
            "Graz-Umgebung",
            "Leoben",
            "Liezen",
            ],
            );
        Game::create_district(&mut map, "Graz(Stadt)", vec!["Graz-Umgebung"]);
        Game::create_district(
            &mut map,
            "Feldkirch",
            vec!["Dornbirn", "Bregenz", "Bludenz"],
            );
        Game::create_district(
            &mut map,
            "Spittal an der Drau",
            vec![
            "Sankt Johann im Pongau",
            "Zell am See",
            "Lienz",
            "Hermagor",
            "Villach Land",
            "Feldkirchen",
            "Murau",
            "Tamsweg",
            ],
            );

        self.map = map;
    }
}
