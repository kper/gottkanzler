use bson;
use failure::{Error, err_msg};
use rand::prelude::*;
use rand::{thread_rng};
use std::collections::HashMap;

mod game;

pub use self::game::Game;

pub type Email = String;
pub type Color = String;
pub type GameId = bson::oid::ObjectId;
pub type Party = String;

const PARTIES: &'static [(&str, &str)] = &[("Neos", "pink"), ("ÖVP", "black"), ("Grünen", "green"), ("Bierpartei", "yellow"), ("Liste Kurz", "#48D1CC"), ("SPÖ", "red"), ("FPÖ", "blue")];
const INFLUENCE_GAIN: i32 = 35;
//const FIELDS_ON_STARTUP: u16 = 10;

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct User {
    pub email: Email,
    pub party: String,
    pub color: Color,
    pub figures: Vec<Position>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Move {
    pub email: String,
    pub party: String,
    pub color: Color,
    pub position: Position,
    /// Reference to old position, when the move fails to know where to rollback
    pub old: Option<Position>
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct District {
    pub name: String,
    pub values: Vec<PartyPresence>,
    #[serde(skip)]
    pub reference_moves: Vec<Move>,
    pub neighbours: Vec<String>
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct PartyPresence {
    pub name: Party,
    pub color: Color,
    pub value: i32,
}

impl User {
    pub fn rollback_move(&mut self, m: Move) -> Result<(), Error> {
        if let Some(new_pos) = m.old {
            let figure = &new_pos.figure;

            let figure_index = self.figures.iter().enumerate().filter(|(index, x)| x.figure == *figure).map(|(index, _)| index).collect::<Vec<_>>()[0];

            self.figures[figure_index] = new_pos.clone();

            Ok(())
        }
        else {
            unreachable!("Move should always be Some");
        }
    }
}

impl District {
    pub fn shrink_presence_except(&mut self, current: &str, players_len: i32) -> Result<(), Error> {
        for party_presence in self.values.iter_mut() {
            let party = &party_presence.name;
            let mut presence = &mut party_presence.value;

            if party == current {
                *presence += INFLUENCE_GAIN;

                if *presence > 100 {
                    *presence = 100;
                }
            } else {
                let old_presence = presence.clone();
                *presence = presence.checked_sub(INFLUENCE_GAIN / players_len).unwrap_or(0);

                if *presence < 0 {
                    *presence = 0;
                }
            }
        }

        Ok(())
    }

    pub fn find_party_in_values(&self, party: String) -> usize {
        let mut index = 0;

        for party_presence in self.values.iter() {
            if party_presence.name == party {
                return index;
            }

            index += 1;
        }

        panic!("This should not happen");
    }

    pub fn find_move_in_values(&self, email: String) -> Vec<(usize, &Move)> {
        self.reference_moves.iter().enumerate().filter(|&(index, r)| r.email == email).collect()
    }
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub enum Figure {
    Turm,
    Reiter,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct Position {
    pub district: String,
    pub figure: Figure,
    pub party: Party,
    pub color: String
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct Score {
    pub party: String,
    pub value: i32,
    pub value_p: String,
    pub color: Color
}

#[cfg(test)]
mod test {
    use super::*;

    fn setup() -> Game {
        let users = vec![
            User {
                email: "1".to_string(),
                party: "ÖVP".to_string(),
                color: "black".to_string(),
                figures: Vec::new(),
            },
            User {
                email: "2".to_string(),
                party: "Neos".to_string(),
                color: "pink".to_string(),
                figures: Vec::new(),
            },
            User {
                email: "3".to_string(),
                party: "Grünen".to_string(),
                color: "green".to_string(),
                figures: Vec::new(),
            },
        ];

        let mut g = Game {
            id: None,
            members: users,
            //players: 3,
            map: Vec::new(),
            queue: Vec::new(),
            admin: "admin@gmail.com".to_string(),
            game_over: false,
            game_started: true
        };

        g.start();

        g
    }

    #[test]
    fn test_rollback_move() {
        let mut user = User {
            email: "1".to_string(),
            party: "ÖVP".to_string(),
            color: "black".to_string(),
            figures: vec![Position {
                district: "Bludenz".to_string(),
                party: "ÖVP".to_string(),
                color: "black".to_string(),
                figure: Figure::Reiter
            }],
        };

        user.rollback_move(Move {
            email: "1".to_string(),
            party: "ÖVP".to_string(),
            color: "black".to_string(),
            position: Position {
                district: "Wien(Stadt)".to_string(),
                party: "ÖVP".to_string(),
                color: "black".to_string(),
                figure: Figure::Reiter
            },
            old: Some(Position {
                district: "Bludenz".to_string(),
                party: "ÖVP".to_string(),
                color: "black".to_string(),
                figure: Figure::Reiter
            })
        });


        assert_eq!(user.figures[0].district, "Bludenz");
    }

    /*
       #[test]
       fn test_party_presence() {
       let game = setup();

       println!("{:?}", game);
       }
       */
    /*

       #[test]
       fn test_turn() {
       let mut game = setup();
       game.queue.push(Move {
       email: "1".to_string(),
       party: "ÖVP".to_string(),
       color: "black".to_string(),
       position: Position {
       district: "Wien(Stadt)".to_string(),
       figure: Figure::Reiter,
       color: "black".to_string(),
       party: "ÖVP".to_string()
       },
       });

       game.apply_queue();

       assert_eq!(game.members[0].figures[0].district, "Wien(Stadt)")
       }

       #[test]
       fn test_update_party_presence() {
       let mut game = setup();

       let position = game.members[0].figures[0].clone();
       let mut clone = game.find_district(&position.district).unwrap();
       let mut control_district = clone.values.iter_mut();

       game.update_party_presence();

       let after_update_district = game.find_district(&position.district).unwrap();

       for (party, party_presence) in after_update_district.values.iter() {
       if *party == game.members[0].party {
       assert_eq!(
     *control_district.next().unwrap() + INFLUENCE_GAIN,
     *party_presence
     );
     } else {
     assert_eq!(
     control_district
     .next()
     .unwrap()
     .checked_sub(2 * INFLUENCE_GAIN)
     .unwrap_or(0),
     *party_presence
     );
     }
     }
     }
     */
}
