use failure::{err_msg, Error};
use mail::Email;

use config::MailConfiguration;
use core::models::Game;
use core::models::User;
use core::stages::session::Session;
use mail::MailAddress;
use mail::Mailbox;
use mail::OutgoingMailbox;
use core::stages::intent::IntentStage;
use std::sync::Arc;
use db::CollectionInterface;

use bson;
use mongodb::db::{Database, ThreadedDatabase};

//pub type Database = Database;

pub enum StageResult {
    Finished(Vec<Email>),
    Unfinished(Email),
}

pub struct StageManager {
    domain: String,
    stage: IntentStage,
    witai_token: String,
}

impl StageManager {
    pub fn new(domain: String, witai_token: &str) -> Self {
        StageManager {
            domain: domain.clone(),
            stage: IntentStage(domain),
            witai_token: witai_token.to_string(),
        }
    }

    pub fn auth_user(
        &self,
        db_connection: &impl CollectionInterface,
        sender: MailAddress,
        ) -> Result<(Game, User), String> {
        let doc = db_connection.find_one(
            doc! {
                "members.email" => (sender.get_inner()),
                "game_over" => false
            },
            );

        //if let Ok(raw_game) = doc {
        if let Some(game) = doc {
            let de_game: Game = bson::from_bson(bson::Bson::Document(game)).unwrap();
            let users: Vec<User> = de_game
                .members
                .iter()
                .cloned()
                .filter(|x| x.email == sender.get_inner())
                .collect();

            if users.len() == 1 {
                Ok((de_game, users[0].clone()))
            } else {
                Err("No or more than one player found in the database for the game".to_string())
            }
        } else {
            Err("Kein Spiel gefunden".to_string())
        }
        /*} else {
          Err("Fehler mongo".to_string())
          }
          */
    }

    pub fn run(&mut self, db_connection: &impl CollectionInterface, incoming: Email, outgoing_mailbox: &Arc<impl OutgoingMailbox>) -> Result<Vec<Email>, Error> {
        info!("Pipeline running");

        let from = incoming.from.clone();

        if from.get_inner() == outgoing_mailbox.get_email() {
            debug!("Email is from the server - ignoring");
            return Ok(Vec::new());
        }

        let (mut game, user) = match self.auth_user(db_connection, from.clone()) {
            Ok(game) => game,
            Err(err) => return Err(err_msg(err)),
        };

        let session = Session {
            witai_token: self.witai_token.clone(),
        };

        let mut email = incoming;
        let mut answers = Vec::new();

        let result = self.stage.run(db_connection, email, &session, &mut game, &user, &outgoing_mailbox)?;

        match result {
            StageResult::Finished(pipeline_answers) => {
                answers = pipeline_answers;
            }
            StageResult::Unfinished(answer) => {
                //continue
                email = answer;
            }
        }

        Ok(answers)
    }

    pub fn next_turn(
        &mut self,
        db_connection: &impl CollectionInterface,
        outgoing_mailbox: &Arc<impl OutgoingMailbox>
    ) -> Result<(), Error> {
        let games: Vec<_> = db_connection
            .find(doc!{});
        //.map(|doc| doc.unwrap())

        for doc in games {
            let mut successful_moves = Vec::new();
            let mut rejected_moves = Vec::new();

            let mut game: Game = bson::from_bson(bson::Bson::Document(doc))?;

            if game.game_over {
                debug!("Game {} is already over - skipping", game.id.unwrap());
                continue;
            }

            if !game.game_started {
                debug!(
                    "Game {} has not been started yet - skipping",
                    game.id.unwrap()
                );
                continue;
            }

            //info!("New for game {}", game.id.clone().unwrap());

            if let Err(err) = game.update_party_presence() {
                eprintln!("{}", err);
                self.send_error(&outgoing_mailbox, &game, &err);
                continue;
            }

            match game.apply_queue(&self.domain.clone()) {
                Err(err) => {
                    self.send_error(&outgoing_mailbox, &game, &err);
                }
                Ok(failed) => {
                    for (to_email, err) in failed.into_iter() {
                        self.send_single_error(&outgoing_mailbox, to_email, &err_msg(err));
                    }
                }
            }

            if let Err(err) = game.resolve_combats(&mut successful_moves, &mut rejected_moves) {
                self.send_error(&outgoing_mailbox, &game, &err);
                continue;
            }

            let is_end = game.check_end();

            if is_end.is_some() {
                game.end();
            }

            if let Err(err) = self
                .update_game(db_connection, &game)
                    .map_err(|err| err_msg("Cannot update the game in the database"))
                    {
                        self.send_error(&outgoing_mailbox, &game, &err);
                        continue;
                    }

            //Report
            for game_move in successful_moves.into_iter() {
                let email = Mailbox::build_email(
                    outgoing_mailbox.get_configuration(),
                    game_move.email.clone(),
                    format!("Zug war erfolgreich - nach {}", game_move.position.district),
                    format!(
                        "Der Zug nach {} war erfolgreich \n\n {}/game/{}",
                        game_move.position.district,
                        self.domain,
                        game.id.clone().unwrap()
                    ),
                    );
                outgoing_mailbox.send(email).unwrap();
            }

            for game_move in rejected_moves.into_iter() {
                let email = Mailbox::build_email(
                    outgoing_mailbox.get_configuration(),
                    game_move.email.clone(),
                    format!("Zug ist gescheitert - nach {}", game_move.position.district),
                    format!(
                        "Der Zug nach {} ist gescheitert",
                        game_move.position.district
                    ),
                    );
                outgoing_mailbox.send(email).unwrap();
            }

            if is_end.is_none() {
                for member in game.members.iter() {
                    let email = Mailbox::build_email(
                        outgoing_mailbox.get_configuration(),
                        member.email.clone(),
                        "Runde abgeschlossen".to_string(),
                        format!("Die neue Runde beginnt \n\n {}/game/{}", self.domain, game.id.clone().unwrap()),
                        );
                    outgoing_mailbox.send(email).unwrap();
                }
            } else {
                let winner = is_end.unwrap();
                for member in game.members.iter() {
                    let email = Mailbox::build_email(
                        outgoing_mailbox.get_configuration(),
                        member.email.clone(),
                        "Game is over".to_string(),
                        format!("Das Spiel ist vorbei! Gewonnen hat {} \n\n {}/game/{}", winner.clone(), self.domain, game.id.clone().unwrap()),
                        );
                    outgoing_mailbox.send(email).unwrap();
                }
            }
        }

        Ok(())
    }

    fn send_single_error(&self, outgoing_mailbox: &Arc<impl OutgoingMailbox>, to_email: String, err: &Error) {
        eprintln!("{}", err);
        let email = Mailbox::build_email(
            outgoing_mailbox.get_configuration(),
            to_email,
            "Fehler - Runde wurde nicht abgeschlossen".to_string(),
            format!("{}", err),
            );
        outgoing_mailbox.send(email).unwrap();
    }

    fn send_error(&self, outgoing_mailbox: &Arc<impl OutgoingMailbox>, game: &Game, err: &Error) {
        eprintln!("{}", err);
        for member in game.members.iter() {
            let email = Mailbox::build_email(
                outgoing_mailbox.get_configuration(),
                member.email.clone(),
                "Fehler - Runde wurde nicht abgeschlossen".to_string(),
                format!("{}", err),
                );
            outgoing_mailbox.send(email).unwrap();
        }
    }

    fn update_game(&self, db_connection: &impl CollectionInterface, game: &Game) -> Result<(), Error> {
        let ser_game = bson::to_bson(&game).unwrap();

        if let bson::Bson::Document(doc) = ser_game {
            let filter = doc! {
                "_id": (game.id.clone().unwrap())
            };

            db_connection
                .replace_one(filter, doc)?;
            Ok(())
        } else {
            Err(err_msg("Game was not a document"))
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use core::stages::intent::IntentStage;
    use core::pipeline::StageManager;
    use mongodb::{Client, ThreadedClient};
    use mail::Email;
    use mail::MockOutgoingMailbox;
    use db::CollectionInterface;
    use db::DummyCollection;
    use lettre::SendableEmail;
    use std;
    use core::models::Figure;
    use core::models::{Position, Move};

    fn setup() -> StageManager {
        let wit_token = "SBM6J5E7RWZHEPV24AWSEV7NEFBWYRUB";
        let mut manager = StageManager::new(wit_token);
        manager
    }

    fn setup_db() -> Client {
        Client::connect("localhost", 27017)
            .expect("Failed to initialize client.")
    }

    fn setup_mailbox() -> MockOutgoingMailbox {
        MockOutgoingMailbox::empty()
    }

    fn setup_game(coll: &impl CollectionInterface, admin: &str, users: Vec<&str>) -> Game {
        let mut game = Game::new(admin.to_string());

        {
            let user = game.add_user(coll, admin.to_string()).unwrap();
            game.members.push(user);
        }


        for user in users.iter() {
            let user = game.add_user(coll, user.to_string()).unwrap();
            game.members.push(user);
        }

        game.start();

        let ser_game = bson::to_bson(&game).unwrap();

        if let bson::Bson::Document(doc)  = ser_game {
            let id = coll.insert_one(doc).unwrap();
            game.id = Some(id);
        }

        game
    }

    #[test]
    fn check_mail_game_without_symbol() {
        let mut manager = setup();
        let mut db = DummyCollection::new();
        let mail = setup_mailbox();
        let game = setup_game(&db, "kevp.per@gmail.com", vec!["stupid@gmail.com"]);

        let mut email = Email::empty().body("Reiter nach Kitzbühel");
        email.from = MailAddress("kevp.per@gmail.com".to_string());
        email.to = MailAddress("server@gottkanzler.net".to_string());

        let emails = manager.run(&db, email, &Arc::new(mail)).unwrap();

        assert!(emails.len() > 0);
        assert_eq!(emails[0].body, "Ich konnte deine Anfrage nicht bearbeiten. Hast du vielleicht vergessen, $ am Kommando-Anfang zu schreiben?".to_string());
    }

    #[test]
    fn check_mail_game_command() {
        let mut manager = setup();
        let mut db = DummyCollection::new();
        let mail = setup_mailbox();
        let game = setup_game(&db, "kevp.per@gmail.com", vec!["stupid@gmail.com"]);

        let mut email = Email::empty().body("$Reiter nach Kitzbühel");
        email.from = MailAddress("kevp.per@gmail.com".to_string());
        email.to = MailAddress("server@gottkanzler.net".to_string());

        let emails = manager.run(&db, email, &Arc::new(mail)).unwrap();

        assert!(emails.len() > 0);
        assert_eq!(emails[0].body, "ok".to_string());
    }

    #[test]
    fn check_mail_game_command_with_space() {
        let mut manager = setup();
        let mut db = DummyCollection::new();
        let mail = setup_mailbox();
        let game = setup_game(&db, "kevp.per@gmail.com", vec!["stupid@gmail.com"]);

        let mut email = Email::empty().body("$ Reiter nach Kitzbühel");
        email.from = MailAddress("kevp.per@gmail.com".to_string());
        email.to = MailAddress("server@gottkanzler.net".to_string());

        let emails = manager.run(&db, email, &Arc::new(mail)).unwrap();

        assert!(emails.len() > 0);
        assert_eq!(emails[0].body, "ok".to_string());
    }

    #[test]
    fn check_mail_game_command_multiple() {
        let mut manager = setup();
        let mut db = DummyCollection::new();
        let mail = setup_mailbox();
        let game = setup_game(&db, "kevp.per@gmail.com", vec!["stupid@gmail.com"]);

        let mut email = Email::empty().body("$ Reiter nach Kitzbühel\n$Turm nach Wien");
        email.from = MailAddress("kevp.per@gmail.com".to_string());
        email.to = MailAddress("server@gottkanzler.net".to_string());

        let emails = manager.run(&db, email, &Arc::new(mail)).unwrap();

        let doc = db.find_one(doc!{"_id": game.id.unwrap()}).unwrap();
        let updated_game : Game = bson::from_bson(bson::Bson::Document(doc)).unwrap();

        assert!(emails.len() > 0);
        assert_eq!(emails.len(), 2);
        assert_eq!(emails[0].body, "ok".to_string());
        assert_eq!(emails[1].body, "ok".to_string());
        assert_eq!(updated_game.queue.len(), 2);
    }

    #[test]
    fn check_mail_game_command_multiple_only_one_correct() {
        let mut manager = setup();
        let mut db = DummyCollection::new();
        let mail = setup_mailbox();
        let game = setup_game(&db, "kevp.per@gmail.com", vec!["stupid@gmail.com"]);

        let mut email = Email::empty().body("$ Reiter nach Kitzbühel\nTurm nach Wien");
        email.from = MailAddress("kevp.per@gmail.com".to_string());
        email.to = MailAddress("server@gottkanzler.net".to_string());

        let emails = manager.run(&db, email, &Arc::new(mail)).unwrap();

        let doc = db.find_one(doc!{"_id": game.id.unwrap()}).unwrap();
        let updated_game : Game = bson::from_bson(bson::Bson::Document(doc)).unwrap();

        assert!(emails.len() > 0);
        assert_eq!(emails.len(), 1);
        assert_eq!(emails[0].body, "ok".to_string());
        assert_eq!(updated_game.queue.len(), 1);
    }

    #[test]
    fn check_mail_game_command_to_wrong_loc() {
        let mut manager = setup();
        let mut db = DummyCollection::new();
        let mail = setup_mailbox();
        let game = setup_game(&db, "kevp.per@gmail.com", vec!["stupid@gmail.com"]);

        let mut email = Email::empty().body("$ Reiter nach München");
        email.from = MailAddress("kevp.per@gmail.com".to_string());
        email.to = MailAddress("server@gottkanzler.net".to_string());

        let emails = manager.run(&db, email, &Arc::new(mail)).unwrap();

        let doc = db.find_one(doc!{"_id": game.id.unwrap()}).unwrap();
        let updated_game : Game = bson::from_bson(bson::Bson::Document(doc)).unwrap();

        assert!(emails.len() > 0);
        assert_eq!(emails[0].body, "ok".to_string());
        assert_eq!(updated_game.queue.len(), 1);
    }

    #[test]
    fn check_mail_game_wrong_queue_next_turn() {
        let mut manager = setup();
        let mut db = DummyCollection::new();
        let mail = Arc::new(setup_mailbox());
        let game = setup_game(&db, "kevp.per@gmail.com", vec!["stupid@gmail.com"]);

        let mut email = Email::empty().body("$ Reiter nach München");
        email.from = MailAddress("kevp.per@gmail.com".to_string());
        email.to = MailAddress("server@gottkanzler.net".to_string());

        let emails = manager.run(&db, email, &mail.clone()).unwrap();

        let doc = db.find_one(doc!{"_id": game.id.unwrap()}).unwrap();
        let updated_game : Game = bson::from_bson(bson::Bson::Document(doc)).unwrap();

        assert!(emails.len() > 0);
        assert_eq!(emails[0].body, "ok".to_string());
        assert_eq!(updated_game.queue.len(), 1);

        //TURN

        manager.next_turn(&db, &mail.clone()).unwrap();

        //println!("{:?}", mail.buffer.borrow().iter().map(|email| std::str::from_utf8(&email.message().unwrap())).collect::<Vec<_>>());

        assert_eq!(mail.buffer.borrow().len(), game.members.len() +1);
    }

    #[test]
    fn check_mail_game_wrong_queue_apply_queue() {
        let mut manager = setup();
        let mut db = DummyCollection::new();
        let mail = Arc::new(setup_mailbox());
        let mut game = setup_game(&db, "kevp.per@gmail.com", vec!["stupid@gmail.com"]);

        let mut email = Email::empty().body("$ Reiter nach München");
        email.from = MailAddress("kevp.per@gmail.com".to_string());
        email.to = MailAddress("server@gottkanzler.net".to_string());

        let emails = manager.run(&db, email, &mail.clone()).unwrap();

        let doc = db.find_one(doc!{"_id": game.id.unwrap()}).unwrap();
        let mut updated_game : Game = bson::from_bson(bson::Bson::Document(doc)).unwrap();

        assert!(emails.len() > 0);
        assert_eq!(emails[0].body, "ok".to_string());
        assert_eq!(updated_game.queue.len(), 1);


        let failed = updated_game.apply_queue().unwrap();
        let email = &failed[0];

        assert_eq!(email.1, "Der Zug ist fehlgeschlagen, weil München nicht erkannt wurde!");
    }

    #[test]
    fn check_mail_game_apply_queue_ok() {
        let mut manager = setup();
        let mut db = DummyCollection::new();
        let mail = Arc::new(setup_mailbox());
        let mut game = setup_game(&db, "kevp.per@gmail.com", vec!["stupid@gmail.com"]);

        let current = &game.members[0].figures[0].district;

        let neighbour = &game.map.iter().filter(|d| d.name == *current).collect::<Vec<_>>()[0].neighbours[0];

        let mut email = Email::empty().body(&format!("$ Reiter nach {}", neighbour));
        email.from = MailAddress("kevp.per@gmail.com".to_string());
        email.to = MailAddress("server@gottkanzler.net".to_string());

        let emails = manager.run(&db, email, &mail.clone()).unwrap();

        let doc = db.find_one(doc!{"_id": game.id.unwrap()}).unwrap();
        let mut updated_game : Game = bson::from_bson(bson::Bson::Document(doc)).unwrap();

        assert!(emails.len() > 0);
        assert_eq!(emails[0].body, "ok".to_string());
        assert_eq!(updated_game.queue.len(), 1);


        let failed = updated_game.apply_queue().unwrap();

        assert_eq!(failed.len(), 0);
    }

    #[test]
    fn check_mail_game_apply_queue_wrong_neighbour() {
        let mut manager = setup();
        let mut db = DummyCollection::new();
        let mail = Arc::new(setup_mailbox());
        let mut game = setup_game(&db, "kevp.per@gmail.com", vec!["stupid@gmail.com"]);

        let current = &game.members[0].figures[0].district;

        let neighbour = &game.map.iter().filter(|d| !d.neighbours.contains(current)).collect::<Vec<_>>()[0].neighbours[0];

        let mut email = Email::empty().body(&format!("$ Reiter nach {}", neighbour));
        email.from = MailAddress("kevp.per@gmail.com".to_string());
        email.to = MailAddress("server@gottkanzler.net".to_string());

        let emails = manager.run(&db, email, &mail.clone()).unwrap();

        let doc = db.find_one(doc!{"_id": game.id.unwrap()}).unwrap();
        let mut updated_game : Game = bson::from_bson(bson::Bson::Document(doc)).unwrap();

        assert!(emails.len() > 0);
        assert_eq!(emails[0].body, "ok".to_string());
        assert_eq!(updated_game.queue.len(), 1);


        let failed = updated_game.apply_queue().unwrap();

        assert_eq!(failed.len(), 1);
        let email = &failed[0];

        assert_eq!(email.1, format!("Der Zug ist fehlgeschlagen, weil es nicht möglich ist von {} nach {} die Figur zu bewegen. Beide Felder liegen nicht nebeneinander.", current, neighbour));

    }

    #[test]
    fn check_mail_game_resolve_combat_ok() {
        let mut manager = setup();
        let mut db = DummyCollection::new();
        let mail = Arc::new(setup_mailbox());
        let mut game = setup_game(&db, "kevp.per@gmail.com", vec!["stupid@gmail.com"]);

        {
            let mut district_1 = &mut game.map[0];
            let district_1_name = district_1.name.clone();

            district_1.reference_moves.push(Move {
                email: "kevp.per@gmail.com".to_string(),
                party: "".to_string(),
                color: "".to_string(),
                position: Position {
                    district: district_1_name,
                    figure: Figure::Reiter,
                    party: "".to_string(),
                    color: "".to_string()
                },
                old: None,
            });
        }

        let mut successful_moves = Vec::new();
        let mut failed_moves = Vec::new();

        game.resolve_combats(&mut successful_moves, &mut failed_moves).unwrap();

        assert_eq!(successful_moves.len(), 1);
        assert_eq!(failed_moves.len(), 0);
    }

    #[test]
    fn check_mail_game_resolve_combat_two_ok() {
        let mut manager = setup();
        let mut db = DummyCollection::new();
        let mail = Arc::new(setup_mailbox());
        let mut game = setup_game(&db, "kevp.per@gmail.com", vec!["stupid@gmail.com"]);

        {
            let mut district = &mut game.map[0];
            let district_name = district.name.clone();

            district.reference_moves.push(Move {
                email: "kevp.per@gmail.com".to_string(),
                party: "".to_string(),
                color: "".to_string(),
                position: Position {
                    district: district_name,
                    figure: Figure::Reiter,
                    party: "".to_string(),
                    color: "".to_string()
                },
                old: None,
            });
        }

        {
            let mut district = &mut game.map[1];
            let district_name = district.name.clone();

            district.reference_moves.push(Move {
                email: "stupid@gmail.com".to_string(),
                party: "".to_string(),
                color: "".to_string(),
                position: Position {
                    district: district_name,
                    figure: Figure::Reiter,
                    party: "".to_string(),
                    color: "".to_string()
                },
                old: None,
            });
        }

        let mut successful_moves = Vec::new();
        let mut failed_moves = Vec::new();

        game.resolve_combats(&mut successful_moves, &mut failed_moves).unwrap();

        assert_eq!(successful_moves.len(), 2);
        assert_eq!(failed_moves.len(), 0);
    }


    #[test]
    fn check_mail_game_resolve_combat_same_district_fail() {
        let mut manager = setup();
        let mut db = DummyCollection::new();
        let mail = Arc::new(setup_mailbox());
        let mut game = setup_game(&db, "kevp.per@gmail.com", vec!["stupid@gmail.com"]);

        let district_1_setup = game.members[0].figures[0].clone();
        let district_2_setup = game.members[1].figures[0].clone();

        {
            let mut district = &mut game.map[0];
            let district_name = district.name.clone();

            district.reference_moves.push(Move {
                email: "kevp.per@gmail.com".to_string(),
                party: "".to_string(),
                color: "".to_string(),
                position: Position {
                    district: district_name,
                    figure: Figure::Reiter,
                    party: "".to_string(),
                    color: "".to_string()
                },
                old: Some(district_1_setup.clone()),
            });
        }

        {
            let mut district = &mut game.map[0];
            let district_name = district.name.clone();

            district.reference_moves.push(Move {
                email: "stupid@gmail.com".to_string(),
                party: "".to_string(),
                color: "".to_string(),
                position: Position {
                    district: district_name,
                    figure: Figure::Reiter,
                    party: "".to_string(),
                    color: "".to_string()
                },
                old: Some(district_2_setup.clone()),
            });
        }

        let mut successful_moves = Vec::new();
        let mut failed_moves = Vec::new();

        game.resolve_combats(&mut successful_moves, &mut failed_moves).unwrap();

        assert_eq!(successful_moves.len(), 0);
        assert_eq!(failed_moves.len(), 2);

        //Check rollback
        assert_eq!(game.members[0].figures[0], district_1_setup);
        assert_eq!(game.members[1].figures[0], district_2_setup);
    }

    #[test]
    fn check_mail_game_resolve_combat_attack_fail() {
        //One figure already on this field and another player attacks

        let mut manager = setup();
        let mut db = DummyCollection::new();
        let mail = Arc::new(setup_mailbox());
        let mut game = setup_game(&db, "kevp.per@gmail.com", vec!["stupid@gmail.com"]);

        let district_setup = game.members[1].figures[0].clone();

        game.members[0].figures[0] = Position {
            district: game.map[0].name.clone(),
            figure: Figure::Reiter,
            party: "".to_string(),
            color: "".to_string()
        };

        {
            let mut district = &mut game.map[0];
            let district_name = district.name.clone();

            district.reference_moves.push(Move {
                email: "stupid@gmail.com".to_string(),
                party: "".to_string(),
                color: "".to_string(),
                position: Position {
                    district: district_name,
                    figure: Figure::Reiter,
                    party: "".to_string(),
                    color: "".to_string()
                },
                old: Some(district_setup.clone()),
            });
        }

        let mut successful_moves = Vec::new();
        let mut failed_moves = Vec::new();

        game.resolve_combats_same_attacking_district_where_figures_already_there(&mut successful_moves, &mut failed_moves).unwrap();

        assert_eq!(successful_moves.len(), 0);
        assert_eq!(failed_moves.len(), 1);

        game.rollback(failed_moves[0].clone()).unwrap();

        assert_eq!(district_setup, game.members[1].figures[0]);
    }
}
