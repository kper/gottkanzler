use failure::{Error, err_msg};
use mail::Email;

use core::models::Game;
use core::models::Move;
use core::models::Position;
use core::models::User;
use core::pipeline::StageResult;
use core::stages::session::Session;

use db::CollectionInterface;
use config::MailConfiguration;
use mail::Mailbox;
use mail::OutgoingMailbox;

use std::sync::Arc;
use witai::*;

type Domain = String;

pub struct IntentStage(pub Domain);

pub fn parse_wit_to_move(user: &User, wit: &Wit) -> Result<Move, Error> {
    let figure = wit.extract_figure()?;
    let new_position = wit.extract_location()?;

    Ok(Move {
        email: user.email.clone(),
        party: user.party.clone(),
        color: user.color.clone(),
        position: Position {
            district: new_position,
            figure: figure,
            party: user.party.clone(),
            color: user.color.clone(),
        },
        old: None
    })
}

impl IntentStage {
    pub fn run(
        &mut self,
        db_connection: &impl CollectionInterface,
        email: Email,
        session: &Session,
        game: &mut Game,
        user: &User,
        mailbox: &Arc<impl OutgoingMailbox>
    ) -> Result<StageResult, Error> {
        info!("Running IntentStage");

        let mut answers = Vec::new();
        let mut game_moves: Vec<Move> = Vec::new();

        let body = email.body.clone();

        for line in body.trim().split('\n') {
            debug!("Line: {}", line);
            if line == "" || line == " " || line.len() == 1 {
                continue;
            }

            if line.chars().take(1).collect::<String>() != "$" {
                continue;
            }

            let answer = email.clone();

            let result = match Client::send(session.witai_token.as_str(), line.clone()) {
                Ok(res) => res,
                Err(err) => {
                    println!("Witai Error {}", err);
                    error!("Witai Error {}", err);
                    answers.push(answer.reply().body(format!(r#"
                        Leider konnte ich das nicht verstehen. Probiere es bitte nochmal!

                        Vergiss nicht, dass Befehle mit einem $ beginnen müssen.

                        Wenn es wieder nicht klappt, gib human@gottkanzler.net bescheid.

                        > {}
                    "#, line).as_str()));

                    return Ok(StageResult::Finished(answers));
                }
            };

            //FIXME error handling
            let intent = result.extract_intent()?;

            match intent.as_str() {
                "Start" => {
                    game.start()?;
                    game.replace(db_connection)?;
                    answers.push(answer.reply().body(format!("> {} \n\n {} \n\n {}/game/{}", line.clone(), "ok", self.0, game.id.clone().unwrap()).as_str()));
                },
                "Move" => {
                    let game_move = parse_wit_to_move(user, &result)?;
                    game.make_move(&email.from, &game_move)?;
                    game.replace(db_connection)?;
                    //game_moves.push(game_move);
                    answers.push(answer.reply().body(format!("> {} \n\n {} \n\n {}/game/{}", line.clone(), "ok", self.0, game.id.clone().unwrap()).as_str()));
                },
                "Surrender" => {
                    game.player_surrenders(email.from.clone())?;
                    game.replace(db_connection)?;
                    answers.push(answer.reply().body(format!("> {} \n\n {} \n\n {}/game/{}", line.clone(), "ok", self.0, game.id.clone().unwrap()).as_str()));
                }
                "Delete_Game" => {
                    if user.email != game.admin {
                        return Err(err_msg(format!("Du musst der Admin sein, um ein Spiel zu löschen!")));
                    }

                    db_connection.delete_one(doc!{
                        "_id": (game.id.clone().unwrap())
                    }).map_err(|err| err_msg(format!("Konnte das Spiel nicht löschen - {}", err)))?;

                    self.inform_all(&mailbox, &game, "Das Spiel wurde gelöscht",
                                    r#"
                        Der Spiel Admin hat das Spiel gelöscht!
                    "#);
                }
                _ => return Err(err_msg(format!("Deine Anweisung macht keinen Sinn für mich. Vergiss nicht, dass ich ein Bot bin :P\nEmpfangen \"{}\". \n\n*Gib einem Human bescheid!*", intent)))
            }

        }

        game.update_moves(db_connection, game_moves, game, user)?;

        if answers.len() == 0 {
            answers.push(email.reply().body("Ich konnte deine Anfrage nicht bearbeiten. Hast du vielleicht vergessen, $ am Kommando-Anfang zu schreiben?"));
        }

        Ok(StageResult::Finished(answers))
    }

    fn inform_all(&self, mailbox: &Arc<impl OutgoingMailbox>, game: &Game, subject: &str, body: &str) {
        for member in game.members.iter() {
            let email = Mailbox::build_email(
                mailbox.get_configuration(),
                member.email.clone(),
                subject.to_string(),
                body.to_string()
            );
            mailbox.send(email).unwrap();
        }
    }
}
