use failure;
use failure::Error;
use reqwest;
use reqwest::header::{Authorization, Bearer};
use core::models::Figure as ModelFigure;

pub struct Client;

#[derive(Debug, Serialize, Deserialize)]
pub struct Wit {
    msg_id: Option<String>,
    _text: Option<String>,
    entities: Entity,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Entity {
    #[serde(skip_serializing_if = "Option::is_none")]
    loc: Option<Vec<Location>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename="Figure")]
    figures: Option<Vec<Figure>>,
    intent: Option<Vec<Intent>>
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Location {
    #[serde(skip_serializing_if = "Option::is_none")]
    suggested: Option<bool>,
    #[serde(skip)]
    confidence: f32,
    value: String,
    #[serde(rename = "type")]
    _type: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Figure {
    #[serde(skip)]
    confidence: f32,
    value: String,
    #[serde(rename = "type")]
    _type: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Intent {
    #[serde(skip)]
    confidence: f32,
    value: String
}

impl Wit {
    pub fn extract_figure(&self) -> Result<ModelFigure, Error> {
        if let Some(ref figures) = self.entities.figures {
            if figures.len() > 1 {
                Err(failure::err_msg("Cannot extract figure because it has more than one"))
            }
            else if figures.len() == 1 {
                let figure = match figures[0].value.as_str() {
                    "Reiter" => ModelFigure::Reiter,
                    "Turm" => ModelFigure::Turm,
                    _ => return Err(failure::err_msg(format!("Cannot recognize figure {}", figures[0].value)))
                };

                Ok(figure)
            }
            else {
                Err(failure::err_msg("Cannot extract figure because it has none"))
            }
        }
        else {
            Err(failure::err_msg("Cannot extract figure because it has none"))
        }
    }

    pub fn extract_location(&self) -> Result<String, Error> {
        if let Some(ref locations) = self.entities.loc {
            if locations.len() > 1 {
                Err(failure::err_msg("Cannot extract location because it has more than one"))
            }
            else if locations.len() == 1 {
                Ok(locations[0].value.clone())
            }
            else {
                Err(failure::err_msg("Cannot extract location because it has no"))
            }
        }
        else {
            Err(failure::err_msg("Cannot extract location because it has none"))
        }
    }

    pub fn extract_intent(&self) -> Result<String, Error> {
        if let Some(ref intents) = self.entities.intent {
            if intents.len() > 1 {
                Err(failure::err_msg("Cannot extract intent because it has more than one"))
            }
            else if intents.len() == 1 {
                Ok(intents[0].value.clone())
            }
            else {
                Err(failure::err_msg("Cannot extract intent because it has no"))
            }
        }
        else {
            Err(failure::err_msg("Ich konnte die notwendigen Parametern nicht extrahieren. Ist das Kommando korrekt?"))
        }
    }
}


impl Client {
    pub fn send(token: &str, text: &str) -> Result<Wit, Error> {
        let header = Authorization(Bearer {
            token: token.to_string(),
        });

        let url = format!("https://api.wit.ai/message?q={}", text);

        let client = reqwest::Client::new();
        //let mut s = String::new();

        let mut raw = client.get(&url).header(header).send()?;

        let response: Wit = raw.json()?;

        eprintln!("{:?}", response);

        //response.read_to_string(&mut s).unwrap();
        //println!("{}", s);

        //println!("{:?}", response);

        //Ok(response.json()?)
        Ok(response)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    use serde_json;

    /*
       #[test]
       fn check_struct_1() {
       let real = json! {
       {
       "msg_id": "0qGlFIWRdRhlGMbn4",
       "_text": "this is my answer",
       "entities": {
       "loc": [{
       "suggested": true,
       "value": "answer",
       "type": "value"
       }]
       }
       }
       };

       let main = Wit {
       msg_id: Some("0qGlFIWRdRhlGMbn4".to_string()),
       _text: Some("this is my answer".to_string()),
       entities: Entity {
       loc: Some(vec![Location {
       suggested: Some(true),
       confidence: 0f32,
       value: "answer".to_string(),
       _type: "value".to_string(),
       }]),
       intent: None,
       figures: None
       },
       };

       let check = serde_json::to_string(&main).unwrap();

       assert_eq!(check, real.to_string());
       }

       #[test]
       fn check_struct_2() {
    //{"_text":"Zug mit Reiter nach Wien","entities":{"Figure":[{"confidence":1,"value":"Reiter","type":"value"}],"loc":[{"confidence":0.98868901143715,"value":"Wien(Stadt)","type":"value"}]},"msg_id":"0u0ITOcDxSTvraeGM"}
    let real = json! {
    {
    "msg_id": "0u0ITOcDxSTvraeGM",
    "_text": "Zug mit Reiter nach Wien",
    "entities": {
    "loc": [{
    "value": "Wien(Stadt)",
    "type": "value"
    }],
    "Figure": [{
    "value": "Reiter",
    "type": "value"
    }]
    }
    }
    };

    let main = Wit {
    msg_id: Some("0u0ITOcDxSTvraeGM".to_string()),
    _text: Some("Zug mit Reiter nach Wien".to_string()),
    entities: Entity {
    figures: Some(vec![Figure {
    confidence: 0f32,
    value: "Reiter".to_string(),
    _type: "value".to_string()
    }]),
    loc: Some(vec![Location {
    suggested: None,
    confidence: 0f32,
    value: "Wien(Stadt)".to_string(),
    _type: "value".to_string()
}]),
    intent: None
    }
};

let check = serde_json::to_string(&main).unwrap();

assert_eq!(check, real.to_string());
}
*/
}
