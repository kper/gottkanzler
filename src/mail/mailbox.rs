use config::MailConfiguration;
use failure;
use failure::err_msg;
use imap::client::Client;
use imap::error::Error as imapError;
use imap::Fetch;
use imap::ZeroCopy;
use lettre;
use lettre::{EmailTransport, SmtpTransport};
use lettre_email;
use lettre_email::EmailBuilder;
use mail::Email;
use mail::MailAddress;
use mailparse::*;
use native_tls::TlsStream;
use std::net::TcpStream;

use mail::OutgoingMailbox;

type Error = failure::Error;

#[derive(Clone)]
pub struct Mailbox {
    config: MailConfiguration,
}

impl OutgoingMailbox for Mailbox {
    fn get_email(&self) -> &str {
        &self.config.email
    }

    fn get_configuration(&self) -> &MailConfiguration {
        &self.config
    }

    fn send(&self, email: lettre_email::Email) -> Result<(), Error> {
        let mut mailer = SmtpTransport::simple_builder(self.config.smtp_domain.as_str())?
            .credentials(lettre::smtp::authentication::Credentials::new(
                    self.config.email.clone(),
                    self.config.password.clone(),
                    ))
            .build();

        mailer.send(&email)?;

        debug!("Email was sent!");

        Ok(())
    }
}

impl Mailbox {
    pub fn new(config: MailConfiguration) -> Self {
        Self {
            config,
        }
    }

    fn select_inbox(imap_socket: &mut Client<TlsStream<TcpStream>>) -> Result<(), Error> {
        match imap_socket.select("INBOX") {
            Ok(_mailbox) => {
                debug!("Inbox selected");
                Ok(())
            }
            Err(_e) => Err(err_msg("Error selecting inbox")),
        }
    }

    pub fn build_email(config: &MailConfiguration, to: String, subject: String, body: String) -> lettre_email::Email {
        EmailBuilder::new()
            .to(to)
            .from(config.email.clone())
            .subject(subject)
            .text(body)
            .build()
            .unwrap()
    }

    pub fn parse(raw_email: &Fetch) -> Option<Email> {
        let m = raw_email.rfc822();

        if m.is_none() {
            return None;
        }

        let parsed = parse_mail(m.unwrap()).unwrap();

        let subject = parsed.headers.get_first_value("Subject").unwrap().unwrap();
        let from = parsed.headers.get_first_value("From").unwrap().unwrap();
        let to = parsed.headers.get_first_value("To").unwrap().unwrap();
        let date = parsed.headers.get_first_value("Date").unwrap().unwrap();
        let msg_id = parsed
            .headers
            .get_first_value("Message-ID")
            .unwrap_or(Some("".to_string()))
            .unwrap_or("".to_string());
        let reply_id = match parsed.headers.get_first_value("In-Reply-To").unwrap() {
            Some(raw) => Some(MailAddress::from(raw).unwrap()),
            None => None,
        };

        if parsed.subparts.len() > 0 {
            let body = parsed.subparts[0].get_body().unwrap();

            let parsed_msg_id = match MailAddress::from(msg_id) {
                Ok(id) => id,
                Err(err) => MailAddress::empty()
            };

            let mail = Email {
                id: parsed_msg_id,
                subject,
                body,
                from: MailAddress::from(from).unwrap(),
                to: MailAddress::from(to).unwrap(),
                parent: reply_id,
                date: Some(date),
            };

            Some(mail)
        }
        else {
            None
        }
    }

    pub fn get_unseen_emails(imap_socket: &mut Client<TlsStream<TcpStream>>) -> Vec<Email> {
        Mailbox::select_inbox(imap_socket).unwrap();

        let mut mails = Vec::new();
        let (num_unseen, ids) = Mailbox::get_unseen_ids(imap_socket);

        debug!("unseen emails {}", num_unseen);

        for id in ids.iter() {
            let raw = Mailbox::fetch_uid(imap_socket, id).unwrap();

            for message in raw.iter() {
                if let Some(mail) = Mailbox::parse(message) {
                    mails.push(mail);
                }
            }
        }

        mails
    }

    fn get_unseen_ids(imap_socket: &mut Client<TlsStream<TcpStream>>) -> (i32, Vec<String>) {
        let mut unseen = imap_socket
            .run_command_and_read_response("UID SEARCH UNSEEN 1:*")
            .unwrap();

        // remove last line of response (OK Completed)
        unseen.pop();

        let mut num_unseen = 0;
        let mut uids = Vec::new();
        let unseen = ::std::str::from_utf8(&unseen[..]).unwrap();

        let unseen = unseen.split_whitespace().skip(2);
        for uid in unseen.take_while(|&e| e != "" && e != "Completed") {
            if let Ok(uid) = usize::from_str_radix(uid, 10) {
                uids.push(format!("{}", uid));
                num_unseen += 1;
            }
        }

        (num_unseen, uids)
    }

    fn fetch_uid(
        imap_socket: &mut Client<TlsStream<TcpStream>>,
        uid: &str,
        ) -> Result<ZeroCopy<Vec<Fetch>>, imapError> {
        imap_socket.uid_fetch(uid, "RFC822")
    }
}
