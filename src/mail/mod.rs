mod mailbox;
mod email;

pub use self::mailbox::Mailbox;
pub use self::email::Email;
pub use self::email::MailAddress;

use config::MailConfiguration;
use lettre_email;
use lettre_email::EmailBuilder;

use failure::Error;
use std::cell::RefCell;

pub trait OutgoingMailbox : Clone {
    fn get_email(&self) -> &str;

    fn get_configuration(&self) -> &MailConfiguration;

    fn reply(&self, answer: Email) -> Result<(), Error> {
        assert!(answer.parent.is_some());

        let email = EmailBuilder::new()
            .to(answer.to.get_inner())
            .from(answer.from.get_inner())
            .subject(answer.subject)
            .text(answer.body)
            .reply_to(answer.from.clone().get_inner())
            .in_reply_to(answer.parent.clone().unwrap().get_inner())
            .reference(answer.parent.unwrap().get_inner())
            .build()
            .unwrap();

        self.send(email).unwrap();

        Ok(())
    }

    fn send_raw_mail(&self, raw_email: Email) -> Result<(), Error> {
        let email = EmailBuilder::new()
            .to(raw_email.to.get_inner())
            .from(raw_email.from.get_inner())
            .subject(raw_email.subject)
            .text(raw_email.body)
            .build()
            .unwrap();

        self.send(email)
    }

    fn send(&self, email: lettre_email::Email) -> Result<(), Error>;

}

#[derive(Clone)]
pub struct MockOutgoingMailbox {
    config: MailConfiguration,
    pub buffer: RefCell<Vec<lettre_email::Email>>
}

impl MockOutgoingMailbox {
    pub fn empty() -> Self {
        let c = MailConfiguration {
            email: "".to_string(),
            password: "".to_string(),
            domain: "".to_string(),
            smtp_domain: "".to_string(),
            port: 0
        };

        MockOutgoingMailbox {
            config: c,
            buffer: RefCell::new(Vec::new())
        }
    }
}

impl OutgoingMailbox for MockOutgoingMailbox {
    fn get_email(&self) -> &str {
        "test"
    }

    fn get_configuration(&self) -> &MailConfiguration {
        &self.config
    }

    fn send(&self, email: lettre_email::Email) -> Result<(), Error> {
        self.buffer.borrow_mut().push(email);
        Ok(())
    }
}
