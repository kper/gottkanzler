use pcre;
use std::fmt;

use failure;
use failure::Error;
use failure::err_msg;

#[derive(Debug, Clone)]
pub struct MailAddress(pub String);

impl MailAddress {
    //kev2908@gmail.com
    //kevin per <kev2908@gmail.com>
    /**
     *  Extracting from the given email string, an parsed email
     * */
    pub fn from(raw: String) -> Result<Self, Error> {
        let mut regex = match pcre::Pcre::compile("(?<=<)(.*)(?=>)|(^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$)") {
            Ok(r) => r,
            Err(_) => {
                return Err(failure::err_msg(format!("Cannot compile regex {}", raw)));
            }
        };

        let cap = regex.exec(raw.as_str()).ok_or_else(|| err_msg("Keine Email im regex gefunden"))?.group(0);

        Ok(MailAddress(cap.to_string()))
    }

    pub fn empty() -> Self {
        MailAddress(String::new())
    }

    pub fn get_inner(&self) -> &str {
        &self.0
    }
}

impl fmt::Display for MailAddress {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[derive(Debug, Clone)]
pub struct Email {
    pub id: MailAddress,
    pub subject: String,
    pub body: String,
    pub from: MailAddress,
    pub to: MailAddress,
    pub parent: Option<MailAddress>,
    pub date: Option<String>,
}

impl Email {
    pub fn empty() -> Self {
        Email {
            id: MailAddress::empty(),
            subject: String::new(),
            body: String::new(),
            from: MailAddress::empty(),
            to: MailAddress::empty(),
            parent: None,
            date: None,
        }
    }

    pub fn get_from(&self) -> MailAddress {
        self.from.clone()
    }

    /**
     * Swaps the sender and the recipient
     * */
    pub fn swap(self) -> Self {
        Email {
            id: MailAddress::empty(),
            subject: self.subject,
            body: self.body,
            from: self.to.clone(),
            to: self.from.clone(),
            parent: None,
            date: None,
        }
    }

    pub fn reply(self) -> Self {
        let parent = self.id.clone();

        let mut answer = self.swap();

        answer.parent = Some(parent);
        answer.subject = format!("Re: {}", answer.subject);
        answer.body = String::new();

        //assert!(answer.parent.is_some());

        answer
    }

    pub fn subject(mut self, sub: &str) -> Self {
        self.subject = sub.to_string();
        self
    }

    pub fn body(mut self, body: &str) -> Self {
        self.body = body.to_string();
        self
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_mail_addresses() {
        let first = "kev2908@gmail.com".into();
        let second = "kevin per <kev2908@gmail.com>".into();
        let third = "<kev2908@gmail.com>".into();

        let mfirst = MailAddress::from(first).unwrap();
        let msecond = MailAddress::from(second).unwrap();
        let mthird = MailAddress::from(third).unwrap();

        assert_eq!(mfirst.0, "kev2908@gmail.com");
        assert_eq!(msecond.0, "kev2908@gmail.com");
        assert_eq!(mthird.0, "kev2908@gmail.com");
    }
}
